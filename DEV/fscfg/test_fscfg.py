#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import numpy
from fscfg import FSCFG
from viterbi_parser import Viterbi_Parser

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: %s [grammar directory] [input string file]" % sys.argv[0]
        exit()
    numpy.seterr(divide='ignore')
    # Testing
    """
    a = Symbol("a")
    b = Symbol("a")
    print "a==a: %s" % (a==b)
    print "a!=a: %s" % (a!=b)
    print "hash(a)==hash(a): %s" % (hash(a)==hash(a))
    """
    # Configure grammar
    grammar_directory = sys.argv[1]
    grammar = FSCFG(features=3)
    grammar.read_terminal_file("%s/terminal.txt" % grammar_directory)
    grammar.read_nonterminal_file("%s/nonterminal.txt" % grammar_directory)
    grammar.read_rule_file("%s/rule.txt" % grammar_directory)
    #grammar.show(0)
    
    # Parsing
    parser = Viterbi_Parser(grammar)
    parser.read_input_string(sys.argv[2])
    parser.initialize_chart()
    #parser.show(0)
    recognize, probabilty = parser.parse()
    parser.show(0)
    print "recognize=%s probabilty=%g" % (recognize, probabilty)
    if recognize: parser.show_parse_tree(parser.parse_tree, 0)
    
    exit()
       
       
       
       
       
       
       
       
       
       
        
