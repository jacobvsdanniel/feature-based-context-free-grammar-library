#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import sys
import copy
import numpy
import scipy
import scipy.stats
from collections import defaultdict

class Nonterminal(object):
    def __init__(self, name):
        self.name = name
        # Record use count in training data for FSCFG learner
        self.count = 0.0
        return

    def show(self, indent):
        print "    "*indent + "nonterminal: [%s] cnt=%g" % (self.name, self.count)

class Terminal(object):
    def __init__(self, name, features, sample_list):
        self.name = name
        self.features = features
        self.feature_weight = 0.0
        self.sample_dict = defaultdict(lambda: 0.0)
        self.samples = 0.0
        self.sample_sum = numpy.zeros(self.features, dtype=float)
        self.sample_square_sum = numpy.zeros(self.features, dtype=float)
        
        # Records for fast calculation of standard deviation of feature vector
        self.sample_sum = numpy.zeros(self.features, dtype=float)
        self.sample_square_sum = numpy.zeros(self.features, dtype=float)
        
        for sample in sample_list:
            self.add_sample(sample)
        return
        
    def add_sample(self, sample):
        # Record to calculate standard deviation of feature vector faster
        sample = tuple(sample)
        self.sample_dict[sample] = self.sample_dict[sample] + 1.0
        self.samples = self.samples + 1.0
        self.sample_sum = self.sample_sum + numpy.array(sample)
        self.sample_square_sum = self.sample_square_sum + numpy.array(sample)**2
        
        feature_mean = self.sample_sum / self.samples
        feature_deviation = numpy.sqrt((1.0/self.samples) * (self.sample_square_sum-2*feature_mean*self.sample_sum) + feature_mean**2)
        self.feature_weight = numpy.log(numpy.prod(1.0 / (feature_deviation + 1.0)))
        return

    def show(self, indent):
        print "    "*indent + "terminal: %s" % self.name
        # feature weights
        print "    "*(indent+1) + "feature weight: %g" % self.feature_weight
        # samples
        for sample, count in self.sample_dict.iteritems():
            output = "    "*(indent+1) + "sample:"
            for feature_value in sample:
                output = output + " %g" % feature_value
            output = output + " [%g]" % count
            print output

class Rule(object):
    def __init__(self):
        self.name = None
        self.left = None
        self.right = []
        self.probability = None
        self.hash_value = None
        # Record used count in training data for FSCFG learner
        self.count = 0.0
        return
    
    def get_clone(self, name_to_symbol=None):
        clone = Rule()
        if name_to_symbol:
            clone.left = name_to_symbol[self.left.name]
            clone.right = [name_to_symbol[symbol.name] for symbol in self.right]
        else:
            clone.left = self.left
            clone.right = self.right[:]
        clone.probability = self.probability
        clone.count = self.count
        return clone
    
    def read_rule_line(self, line, name_to_symbol):
        string_list = line.strip().split(" ")
        self.name = string_list[0]
        self.left = name_to_symbol[string_list[1]]
        self.right = [name_to_symbol[name] for name in string_list[2:-1]]
        self.probability = numpy.log(float(string_list[-1]))
        return
        
    def compute_hash(self):
        self.name = "".join([self.left.name]+[symbol.name for symbol in self.right])
        self.hash_value = hash(self.name)
        
    def __eq__(self, other):
        return self.name == other.name
        
    def __ne__(self, other):
        return not(self == other)
        
    def __hash__(self):
        return self.hash_value
    
    def show(self, indent):
        output = "    "*indent + "[%s] %s -> " % (self.name, self.left.name)
        output = output + " ".join([symbol.name for symbol in self.right])
        output = output + " [%g] cnt=%g" % (numpy.exp(self.probability), self.count)
        print output
        return

class FSCFG(object):
    def __init__(self, features):
        self.terminal_set = set()
        self.nonterminal_set = set()
        self.start_symbol = None
        self.rule_dict = {}
        self.rhs_rule_dict = {}
        self.features = features
        self.feature_deviation = [10.0 for i in range(self.features)]
        
        # Store next-to-use name suffixes
        self.nonterminal_index = 0
        self.terminal_index = 0
        
        # A 2D dict representing probabilistic reflexive and transitive left corner relation
        self.left_relation = None

        # A dict storing epsilon probabilities
        self.epsilon_rule_probability = None
        self.epsilon_expansion_probability = None

        # Map symbol name to reference
        self.name_to_symbol = {}
        
        # Map rule name to reference
        self.name_to_rule = {}
        return
    
    def get_clone(self):
        clone = FSCFG(self.features)
        clone.terminal_set = copy.deepcopy(self.terminal_set)
        clone.nonterminal_set = copy.deepcopy(self.nonterminal_set)
        
        for symbol in clone.terminal_set:
            clone.name_to_symbol[symbol.name] = symbol
        for symbol in clone.nonterminal_set:
            clone.name_to_symbol[symbol.name] = symbol
        
        clone.start_symbol = clone.name_to_symbol[self.start_symbol.name]
        clone.nonterminal_index = self.nonterminal_index
        clone.terminal_index = self.terminal_index
        
        for nonterminal, rule_set in self.rule_dict:
            clone_nonterminal = clone.name_to_symbol[nonterminal.name]
            clone.rule_dict[clone_nonterminal] = set()
            for rule in rule_set:
                clone_rule = rule.get_clone(clone.name_to_symbol)
                clone_rule.compute_hash()
                clone.rule_dict[clone_rule.left].add(clone_rule)
                clone.name_to_rule[clone_rule.name] = clone_rule
                
        for nonterminal, rule_index_set in self.rhs_rule_dict:
            clone_nonterminal = clone.name_to_symbol[nonterminal.name]
            clone.rhs_rule_dict[clone_nonterminal] = set([(clone.name_to_rule[rule.name], index) for rule, index in rule_index_set])
        
        clone.feature_deviation = numpy.copy(self.feature_deviation)
        return clone
        
    def read_terminal_file(self, input_file):
        self.terminal_set.clear()
        with open(input_file, "r") as f:
            for line in f.readlines():
                part_list = line.strip().split("|")
                sample_list = []
                for part in part_list[1:]:
                    sample = [float(feature_value) for feature_value in part.strip().split(" ")]
                    if len(sample) != self.features:
                        raise AssertionError("Unexpected sample length: %d" % len(sample))
                    sample_list.append(sample)
                terminal = Terminal(part_list[0].strip(), self.features, sample_list)
                self.terminal_set.add(terminal)
                self.name_to_symbol[terminal.name] = terminal
        return

    def read_nonterminal_file(self, input_file):
        self.start_symbol = None
        self.nonterminal_set.clear()
        index = -1
        with open(input_file, "r") as f:
            for line in f.readlines():
                index = index + 1
                nonterminal = Nonterminal(line.strip())
                self.nonterminal_set.add(nonterminal)
                self.name_to_symbol[nonterminal.name] = nonterminal
                # Set the first as start symbol
                if not self.start_symbol:
                    self.start_symbol = nonterminal
        return

    def read_rule_file(self, input_file):
        """ Not storing epsilon rules in rule_dict """
        self.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in self.nonterminal_set}
        self.rule_dict.clear()
        self.rhs_rule_dict.clear()
        self.name_to_rule.clear()
        rule_dict = {nonterminal: set() for nonterminal in self.nonterminal_set}
        rhs_rule_dict = {nonterminal: set() for nonterminal in self.nonterminal_set}
        rhs_rule_dict.update({terminal: set() for terminal in self.terminal_set})
        name_to_rule = {}

        with open(input_file, "r") as f:
            for line in f.readlines():
                rule = Rule()
                rule.read_rule_line(line, self.name_to_symbol)
                rule.compute_hash()
        
                if not rule.right:
                    self.epsilon_rule_probability[rule.left] = rule.probability
                else:
                    rule_dict[rule.left].add(rule)
                    for rhs_index, symbol in enumerate(rule.right):
                        rhs_rule_dict[symbol].add((rule, rhs_index))
                    name_to_rule[rule.name] = rule
                
        for nonterminal, rule_set in rule_dict.iteritems():
            total_probability = sum([numpy.exp(rule.probability) for rule in rule_set])
            total_probability = total_probability + numpy.exp(self.epsilon_rule_probability[nonterminal])
            if not numpy.isclose([total_probability], [1]):
                raise AssertionError("Total probability for LHS [%s] is %g, not unity" % (nonterminal.name, total_probability))

        self.rule_dict = rule_dict
        self.rhs_rule_dict = rhs_rule_dict
        self.name_to_rule = name_to_rule
        return
    
    def add_nonterminal(self):
        """ Add a nonterminal and return its reference """
        nonterminal = Nonterminal("N%d" % self.nonterminal_index)
        self.nonterminal_index = self.nonterminal_index + 1
        self.nonterminal_set.add(nonterminal)
        
        self.name_to_symbol[nonterminal.name] = nonterminal
        self.rule_dict[nonterminal] = set()
        self.rhs_rule_dict[nonterminal] = set()
        return nonterminal
        
    def remove_nonterminal(self, nonterminal):
        self.nonterminal_set.remove(nonterminal)
        del self.name_to_symbol[nonterminal.name]
        del self.rule_dict[nonterminal]
        del self.rhs_rule_dict[nonterminal]
        return
        
    def add_terminal(self, sample_list):
        """ Add a terminal and return its reference """
        terminal = Terminal("t%d" % self.terminal_index, self.features, sample_list)
        self.terminal_index = self.terminal_index + 1
        self.terminal_set.add(terminal)
        self.name_to_symbol[terminal.name] = terminal
        self.rhs_rule_dict[terminal] = set()
        return terminal
    
    def remove_terminal(self, terminal):
        self.terminal_set.remove(terminal)
        del self.name_to_symbol[terminal.name]
        del self.rhs_rule_dict[terminal]
        return
    
    def insert_rule(self, rule):
        self.rule_dict[rule.left].add(rule)
        for rhs_index, symbol in enumerate(rule.right):
            self.rhs_rule_dict[symbol].add((rule, rhs_index))
        self.name_to_rule[rule.name] = rule
        return
    
    def delete_rule(self, rule):
        self.rule_dict[rule.left].discard(rule)
        for rhs_index, symbol in enumerate(rule.right):
            self.rhs_rule_dict[symbol].discard((rule, rhs_index))
        del self.name_to_rule[rule.name]
        return
    
    def add_lexical_rule(self, sample, left=None):
        """ Add the lexical rule N_i -> t_j [1] with only one sample for t_j """
        rule = Rule()
        if left:
            rule.left = left
        else:
            rule.left = self.add_nonterminal()
            rule.left.count = 1.0
        rule.right = [self.add_terminal([sample])]
        rule.probability = 0.0
        rule.count = 1.0
        rule.compute_hash()
        self.rule_dict[rule.left].add(rule)
        self.rhs_rule_dict[rule.right[0]].add((rule,0))
        self.name_to_rule[rule.name] = rule
        return rule
    
    def add_naive_rule(self, sample_string):
        """ Add the naive rule S -> N_1 N_2 ... N_m [1] """
        rhs = []
        for sample in sample_string:
            lexical_rule = self.add_lexical_rule(sample)
            rhs.append(lexical_rule.left)

        rule = Rule()
        rule.left = self.start_symbol
        rule.right = rhs
        rule.probability = 0.0
        rule.left.count = rule.left.count + 1.0
        rule.count = 1.0
        rule.compute_hash()
        self.insert_rule(rule)
        return rule
    
    def add_skip_and_epsilon(self):
        """ Decorations which will only be added right before parser initialization and will be removed right after tracing parse tree """
        
        self.skip_nonterminal = self.add_nonterminal()
        self.skip_terminal = self.add_terminal([])
        self.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in self.nonterminal_set}
        
        # Bookkeeping for latter deletion
        self.skip_rule_set = set()
        
        # Add rule: START -> START SKIP
        skip_rule = Rule()
        skip_rule.left = self.start_symbol
        skip_rule.right = [self.start_symbol, self.skip_nonterminal]
        skip_rule.probability = self.features * -10.0 #TODO: Tune insertion error probability
        skip_rule.compute_hash()
        self.rule_dict[skip_rule.left].add(skip_rule)
        self.skip_rule_set.add(skip_rule)
        
        # Add rules: N_preterminal -> SKIP N_preterminal
        # Add null productions for N_preterminals
        for terminal in self.terminal_set:
            try:
                preterminal = next(iter(self.rhs_rule_dict[terminal]))[0].left
            except StopIteration:
                continue
            skip_rule = Rule()
            skip_rule.left = preterminal
            skip_rule.right = [self.skip_nonterminal, preterminal]
            skip_rule.probability = self.features * -10.0 #TODO: Tune insertion error probability
            skip_rule.compute_hash()
            self.rule_dict[skip_rule.left].add(skip_rule)
            self.skip_rule_set.add(skip_rule)
            self.epsilon_rule_probability[preterminal] = self.features * -10.0 #TODO: Tune deletion error probability
        
        # Add rule: SKIP -> skip
        skip_rule = Rule()
        skip_rule.left = self.skip_nonterminal
        skip_rule.right = [self.skip_terminal]
        skip_rule.probability = 0.0
        skip_rule.compute_hash()
        self.rule_dict[skip_rule.left].add(skip_rule)
        return
    
    def remove_skip_and_epsilon(self):
        self.remove_nonterminal(self.skip_nonterminal)
        self.skip_nonterminal = None
        self.remove_terminal(self.skip_terminal)
        self.skip_terminal = None
        self.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in self.nonterminal_set}
        for rule in self.skip_rule_set:
            self.rule_dict[rule.left].remove(rule)
        
    def compute_rule_probability(self):
        for nonterminal, rule_set in self.rule_dict.iteritems():
            for rule in rule_set:
                rule.probability = numpy.log(rule.count/nonterminal.count)
        return
    
    def compute_left_relation(self):
        self.left_relation = {}
        for N_i in self.nonterminal_set:
            self.left_relation[N_i] = {N_j: float("-inf") for N_j in self.nonterminal_set}
        
        # Compute probabilistic left corner relation
        for N_i in self.nonterminal_set:
            for rule in self.rule_dict[N_i]:
                N_j = rule.right[0]
                if N_j not in self.nonterminal_set: continue
                if rule.probability > self.left_relation[N_i][N_j]:
                    self.left_relation[N_i][N_j] = rule.probability

        # Compute transitive relation with Floyd-Warshall
        for N_k in self.nonterminal_set:
            for N_i in self.nonterminal_set:
                for N_j in self.nonterminal_set:
                    self.left_relation[N_i][N_j] = max(self.left_relation[N_i][N_j], self.left_relation[N_i][N_k]+self.left_relation[N_k][N_j])

        # Get reflexive relation
        for nonterminal in self.nonterminal_set:
            self.left_relation[nonterminal][nonterminal] = 0.0
        
        print "left relation probability:"
        output = "    "
        for N_i in self.nonterminal_set:
            output = "    %4s:" % N_i.name
            for N_j in self.nonterminal_set:
                output = output + " %10g" % self.left_relation[N_i][N_j]
            print output
        return

    def compute_epsilon_expansion(self, improvement_bound=1e-10):
        self.epsilon_expansion_probability = copy.copy(self.epsilon_rule_probability)
        
        # Get a rule_dict of pure nonterminal rules
        rule_dict = {nonterminal: set() for nonterminal in self.nonterminal_set}
        for nonterminal, rule_set in self.rule_dict.iteritems():
            for rule in rule_set:
                has_terminal = False
                for symbol in rule.right:
                    if symbol in self.terminal_set:
                        has_terminal = True
                        break
                if not has_terminal:
                    rule_dict[nonterminal].add(rule)
                    
        # Compute epsilon expansion
        while True:
            #print self.epsilon_expansion_probability
            print "epsilon expansion probability:"
            output = "    "
            for nonterminal, probability in self.epsilon_expansion_probability.iteritems():
                output = output + "%s=%g " % (nonterminal.name, probability) 
            print output
        
            converge = True
            for nonterminal in self.nonterminal_set:
                update_value = self.epsilon_rule_probability[nonterminal]
                for rule in rule_dict[nonterminal]:
                    rhs_epsilon_probability = sum([self.epsilon_expansion_probability[symbol] for symbol in rule.right])
                    update_value = max(update_value, rule.probability+rhs_epsilon_probability)
                if update_value!=float("-inf") and numpy.exp(update_value) - numpy.exp(self.epsilon_expansion_probability[nonterminal]) > improvement_bound:
                    print nonterminal.name
                    print update_value
                    print self.epsilon_expansion_probability[nonterminal]
                    converge = False
                self.epsilon_expansion_probability[nonterminal] = update_value
            if converge: break

        print "epsilon expansion probability:"
        output = "    "
        for nonterminal, probability in self.epsilon_expansion_probability.iteritems():
            output = output + "%s=%g " % (nonterminal.name, probability) 
        print output
        return

    def scan_probability(self, terminal, input_sample):
        # skip_terminal
        if terminal.samples == 0:
            return 0 #TODO: Tune insertion error probability
        
        probability_list = []
        for sample, sample_weight in terminal.sample_dict.iteritems():
            probability = numpy.sum(numpy.log(scipy.stats.norm(sample,self.feature_deviation).pdf(input_sample)))
            probability_list.append(numpy.log(sample_weight) + probability)

        probability_list = numpy.array(probability_list)
        probability_max = numpy.max(probability_list)
            
        offset = 0
        if probability_max < -600:
            offset = 600 - probability_max
            probability_list = probability_list + offset
        
        return numpy.log(numpy.sum(numpy.exp(probability_list))) - offset + terminal.feature_weight*len(terminal.sample_dict) - numpy.log(terminal.samples)
        
    def show(self, indent):
        print "    "*indent + "FSCFG:"
        print "    "*(indent+1) + "feature deviation array:"
        print "    "*(indent+2) + " ".join([str(value) for value in self.feature_deviation])
        
        print "    "*(indent+1) + "terminal list:"
        for terminal in self.terminal_set:
            terminal.show(indent+2)

        print "    "*(indent+1) + "nonterminal list:"
        for nonterminal in self.nonterminal_set:
            nonterminal.show(indent+2)

        print "    "*(indent+1) + "start symbol:"
        self.start_symbol.show(indent+2)

        print "    "*(indent+1) + "rule:"
        for nonterminal, rule_set in self.rule_dict.iteritems():
            nonterminal.show(indent+2)
            for rule in rule_set:
                rule.show(indent+3)
                
        print "    "*(indent+1) + "right-hand-side rule:"
        for nonterminal, rule_index_set in self.rhs_rule_dict.iteritems():
            nonterminal.show(indent+2)
            for rule, index in rule_index_set:
                rule.show(indent+3)
                print "    "*(indent+3) + "index=%d" % index
        return











        
