import re
import sys
import copy
from fscfg import Nonterminal, Terminal, Rule, FSCFG
import numpy
import scipy
import scipy.stats

class State(object):
    def __init__(self, rule=None, next_index=None, start_index=None, next_rhs_index=None, forward_probability=None, viterbi_probability=None, predecesser=None):
        self.rule = rule

        # Index of next symbol to scan in input string (i)
        self.next_index = next_index

        # Index of the start of LHS in input string (k)
        self.start_index = start_index

        # Index of next right-hand-side symbol (.)
        self.next_rhs_index = next_rhs_index

        # Reference of predecesser state (two states for completed state)
        self.predecesser = predecesser

        # viterbi probability (v)
        self.viterbi_probability = viterbi_probability

        # "viterbi forward" probability
        self.forward_probability = forward_probability
        return

    def __eq__(self, other):
        return (self.rule==other.rule) and (self.next_index==other.next_index) and (self.start_index==other.start_index) and (self.next_rhs_index==other.next_rhs_index)

    def __ne__(self, other):
        return not (self == other)

    def __hash__(self):
        return hash((self.rule, self.next_index, self.start_index, self.next_rhs_index))

    def show(self, indent):
        output = "    "*indent + "%d:%d_%s ->" % (self.next_index, self.start_index, self.rule.left.name)
        find_dot = False
        for i in range(len(self.rule.right)):
            if (not find_dot) and (i==self.next_rhs_index):
                output = output + " ."
                find_dot = True
            output = output + " %s" % self.rule.right[i].name
        if not find_dot: output = output + " ."
        output = output + " [%g, %g]" % (self.forward_probability, self.viterbi_probability)
        print output
        return

class Viterbi_Parser(object):
    def __init__(self, grammar=None, scan_tolerance=0.5):
        self.grammar = grammar
        self.grammar.compute_left_relation()
        self.grammar.compute_epsilon_expansion()
        self.scan_probability_threshold = numpy.log(scipy.stats.norm(0,1).pdf(scan_tolerance)) * self.grammar.features
        return

    def read_input_string(self, input_file):
        self.sample_string = []
        with open(input_file, "r") as f:
            for line in f.readlines():
                string_list = line.strip().split(" ")
                sample = [float(feature_value) for feature_value in string_list]
                self.sample_string.append(sample)

    def initialize_chart(self):
        # chart[0] ~ chart[string_length]
        string_length = len(self.sample_string)
        self.state_chart = [[] for i in range(string_length+1)]

        # Add dummy state 0:0_""->.S[1]
        dummy_rule = Rule()
        dummy_rule.left = Nonterminal("")
        dummy_rule.right = [self.grammar.start_symbol]
        dummy_rule.probability = 0.0
        dummy_rule.compute_hash()
        dummy_state = State(rule=dummy_rule, next_index=0, start_index=0, next_rhs_index=0, forward_probability=0, viterbi_probability=0)
        self.state_chart[0].append(dummy_state)

        # Add a "list of dict{state:index}" for fast existence testing and index lookup
        self.state_to_index = [{} for i in range(string_length+1)]
        self.state_to_index[0][dummy_state] = 0

        # Do spontaneous dot shifting as in predictor
        self.spontaneous_dot_shifter(dummy_state)
        return

    def show(self, indent):
        self.grammar.show(indent)

        # samples
        print "    "*indent + "sample string:"
        for sample in self.sample_string:
            output = "    "*(indent+1) + "sample:"
            for feature_value in sample:
                output = output + " %g" % feature_value
            print output

        print "    "*indent + "chart:"
        for i in range(len(self.state_chart)):
            print "    "*(indent+1) + "[%d]" % i
            for state in self.state_chart[i]:
                state.show(indent+2)
        return

    def insert_state(self, state, check_probability=True):
        if state not in self.state_to_index[state.next_index]:
            self.state_to_index[state.next_index][state] = len(self.state_chart[state.next_index])
            self.state_chart[state.next_index].append(state)
        elif check_probability:
            index = self.state_to_index[state.next_index][state]
            if self.state_chart[state.next_index][index].viterbi_probability < state.viterbi_probability:
                self.state_chart[state.next_index][index].viterbi_probability = state.viterbi_probability
                self.state_chart[state.next_index][index].forward_probability = state.forward_probability
                self.state_chart[state.next_index][index].predecesser = state.predecesser
        return
    """
    def predictor(self, state):
        nonterminal = state.rule.right[state.next_rhs_index]
        for rule in self.grammar.rule_dict[nonterminal]:
            predicted_state = State()
            predicted_state.predecesser = state
            predicted_state.rule = rule
            predicted_state.next_index = state.next_index
            predicted_state.start_index = state.next_index
            predicted_state.next_rhs_index = 0
            predicted_state.viterbi_probability = rule.probability
            self.insert_state(predicted_state, check_probability=False)
            self.spontaneous_dot_shifter(predicted_state)
        return
    """
    def predictor(self, state):
        """ Collapse left recursion predictor """
        nonterminal = state.rule.right[state.next_rhs_index]
        for left_relation_nonterminal in self.grammar.nonterminal_set:
            left_relation_probability = self.grammar.left_relation[nonterminal][left_relation_nonterminal]
            if numpy.isclose(left_relation_probability, float("-inf")): continue
            for rule in self.grammar.rule_dict[left_relation_nonterminal]:
                predicted_state = State()
                predicted_state.predecesser = state
                predicted_state.rule = rule
                predicted_state.next_index = state.next_index
                predicted_state.start_index = state.next_index
                predicted_state.next_rhs_index = 0
                predicted_state.viterbi_probability = rule.probability
                predicted_state.forward_probability = state.forward_probability + left_relation_probability + rule.probability

                self.insert_state(predicted_state, check_probability=True)
                self.spontaneous_dot_shifter(predicted_state)
        return

    def scanner(self, state):
        #if state.rule.right[state.next_rhs_index] != self.symbol_string[state.next_index]: return
        scan_probability = self.grammar.scan_probability(state.rule.right[state.next_rhs_index], self.sample_string[state.next_index])
        if scan_probability < self.scan_probability_threshold: return
        scanned_state = copy.copy(state)
        scanned_state.predecesser = state
        scanned_state.next_index = state.next_index + 1
        scanned_state.next_rhs_index = state.next_rhs_index + 1
        scanned_state.viterbi_probability = scanned_state.viterbi_probability + scan_probability
        scanned_state.forward_probability = scanned_state.forward_probability + scan_probability
        self.insert_state(scanned_state, check_probability=True)
        return

    def completer(self, state):
        for old_state in self.state_chart[state.start_index]:
            if (old_state.next_rhs_index == len(old_state.rule.right)) or (old_state.rule.right[old_state.next_rhs_index] != state.rule.left): continue
            completed_state = copy.copy(old_state)
            completed_state.predecesser = (state, old_state)
            completed_state.next_index = state.next_index
            completed_state.next_rhs_index = completed_state.next_rhs_index + 1
            completed_state.viterbi_probability = old_state.viterbi_probability + state.viterbi_probability
            completed_state.forward_probability = old_state.forward_probability + state.viterbi_probability
            self.insert_state(completed_state, check_probability=True)
            self.spontaneous_dot_shifter(completed_state)
        return

    def spontaneous_dot_shifter(self, state):
        old_state = state
        while True:
            if old_state.next_rhs_index == len(old_state.rule.right): break
            skip_symbol = old_state.rule.right[old_state.next_rhs_index]
            if skip_symbol not in self.grammar.nonterminal_set: break
            skip_probability = self.grammar.epsilon_expansion_probability[skip_symbol]
            if numpy.isclose(skip_probability, float("-inf")): break

            shifted_state = copy.copy(old_state)
            shifted_state.predecesser = [None, old_state]
            shifted_state.next_rhs_index = old_state.next_rhs_index + 1
            shifted_state.viterbi_probability = old_state.viterbi_probability + skip_probability
            shifted_state.forward_probability = old_state.forward_probability + skip_probability

            self.insert_state(shifted_state, check_probability=True)
            old_state = shifted_state
        return

    def parse(self):
        for row in range(len(self.state_chart)):
            for state in self.state_chart[row]:
                if state.next_rhs_index == len(state.rule.right):
                    #if state.rule.left==Symbol(""):
                    if row==len(self.state_chart)-1 and state.rule.left.name=="":
                        self.parse_tree = self.back_trace(state)[1][0]
                        return True, state.viterbi_probability
                    self.completer(state)
                elif row != len(self.state_chart)-1:
                    if state.rule.right[state.next_rhs_index] in self.grammar.nonterminal_set:
                        self.predictor(state)
                    else:
                        self.scanner(state)
            #print "chart[%d] has %d states" % (row, len(self.state_chart[row]))
        return False, 0

    def back_trace(self, state):
        if state.next_rhs_index == 0:
            # Add applied rule to the tree node and form a tuple (nonterminal, rule)
            return [(state.rule.left, state.rule), []]

        previous_rhs_symbol = state.rule.right[state.next_rhs_index-1]

        if previous_rhs_symbol in self.grammar.terminal_set:
            tree = self.back_trace(state.predecesser)
            tree[1].append([(previous_rhs_symbol, None),[]])
            return tree
        else:
            tree = self.back_trace(state.predecesser[1])
            if state.predecesser[0]:
                child_tree = self.back_trace(state.predecesser[0])
            else:
                child_tree = [(previous_rhs_symbol, None), []]
            tree[1].append(child_tree)
            return tree

    def show_parse_tree(self, tree, indent):
        tree[0][0].show(indent)
        # show the applyed rule of the nonterminal
        if tree[0][1]:
            tree[0][1].show(indent)
        for child_tree in tree[1]:
            self.show_parse_tree(child_tree, indent+1)
        return










        
