#!/usr/bin/python 
# -*- coding: utf-8 -*-

import re
import sys
import os 
import copy
import numpy
from fscfg import Nonterminal, Terminal, Rule, FSCFG
from viterbi_parser import Viterbi_Parser

class FSCFG_Learner(object):
    def __init__(self, features, parse_probability_threshold=float("-inf")):
        self.grammar = None
        self.features = features
        self.parse_probability_threshold = parse_probability_threshold
        
        # Store training data with 2 dim list indexed with [i][j], i: # strings to train, j: # samples in a string 
        self.training_data = []
        return

    def read_training_data(self, input_path):
        file_list = []
        for entry in os.listdir(input_path):
            entry_path = os.path.join(input_path, entry)
            if os.path.isfile(entry_path):
                file_list.append(entry_path)
        for file_path in sorted(file_list):
            with open(file_path, "r") as f:
                sample_string = []
                for line in f.readlines():
                    string_list = line.strip().split(" ")
                    sample = [float(feature_value) for feature_value in string_list]
                    sample_string.append(sample)
                self.training_data.append(sample_string)
        
        # Normalize each feature to standard gaussion distribution
        self.training_data = self.training_data - numpy.mean(self.training_data, axis=(0,1))
        self.training_data = self.training_data / numpy.std(self.training_data, axis=(0,1))
        return 

    def trace_parse_tree(self, tree, sample_string, sample_index):
        symbol = tree[0][0]
        
        # Node: terminal
        if symbol in self.grammar.terminal_set:
            symbol.add_sample(sample_string[sample_index[0]])
            sample_index[0] = sample_index[0] + 1
            return
        
        rule = tree[0][1]
        
        # Node: nonterminal to epsilon
        if not rule: return
                
        # Node: nonterminal with normal rule or skip rule
        if rule not in self.grammar.skip_rule_set:
            # Normal rules
            rhs = []
            for child_tree in tree[1]:
                child_symbol = child_tree[0][0]
                if child_symbol in self.grammar.terminal_set or child_tree[0][1]:
                    rhs.append(child_symbol)
                    self.trace_parse_tree(child_tree, sample_string, sample_index)
                
            if len(rhs) == len(rule.right):
                rule.count = rule.count + 1.0
                rule.left.count = rule.left.count + 1.0
            else:
                # Some child nonterminals expand to epsilon
                modified_rule = rule.get_clone()
                modified_rule.right = rhs
                modified_rule.count = 1.0
                modified_rule.left.count = modified_rule.left.count + 1.0
                modified_rule.compute_hash()
                if modified_rule not in self.grammar.rule_dict[modified_rule.left]:
                    self.grammar.insert_rule(modified_rule)
                else:
                    existing_rule = self.grammar.name_to_rule[modified_rule.name]
                    existing_rule.count = existing_rule.count + modified_rule.count
        else:
            # N -> SKIP N or S -> S SKIP
            if rule.left != self.grammar.start_symbol:
                skip_rhs_index = 0
                skip_sample_index = sample_index[0]
                sample_index[0] = sample_index[0] + 1
            else:
                skip_rhs_index = 1
                skip_sample_index = sample_index[1]
                sample_index[1] = sample_index[1] - 1
            
            new_lexical_rule = self.grammar.add_lexical_rule(sample_string[skip_sample_index])
            modified_rule = rule.get_clone()
            modified_rule.right[skip_rhs_index] = new_lexical_rule.left
            modified_rule.count = 1.0
            modified_rule.left.count = modified_rule.left.count + 1.0
            
            child_tree = tree[1][1-skip_rhs_index]
            if child_tree[0][1]:
                self.trace_parse_tree(child_tree, sample_string, sample_index)
            else:
                # when child preterminal expand to epsilon
                modified_rule.right = [new_lexical_rule.left]
            modified_rule.compute_hash()
            self.grammar.insert_rule(modified_rule)
        return

    def merge(self, X, Y):
        """ Given nonterminal X and Y, generate new nonterminal Z = merge(X, Y) """
        Z = self.grammar.add_nonterminal()

        # If X or Y is start symbol, new nonterminal should be the start symbol
        if self.grammar.start_symbol == X or self.grammar.start_symbol == Y:
            self.grammar.start_symbol = Z

        # {to_be_replaced_rule: [is_lhs_replaced, rhs_index_list]}
        replaced_rule_dict = defaultdict(lambda: [False, []])
        for rule in self.grammar.rule_dict[X]:
            replaced_rule_dict[rule][0] = True
        for rule in self.grammar.rule_dict[Y]:
            replaced_rule_dict[rule][0] = True
        for rule, rhs_index in self.grammar.rhs_rule_dict[X]:
            replaced_rule_dict[rule][1].append(rhs_index)
        for rule, rhs_index in self.grammar.rhs_rule_dict[Y]:
            replaced_rule_dict[rule][1].append(rhs_index)
        
        lexical_samples = []
        lexical_rule_count = 0.0
        for rule, (is_lhs_replaced, rhs_index_list) in replaced_rule_dict.iteritems():
            # Handle lexical rule
            if rule.right[0] in self.grammar.terminal_set:
                lexical_rule_count = lexical_rule_count + rule.count
                lexical_samples = lexical_samples + rule.right[0].sample_dict.keys() # TODO:weight
                self.grammar.delete_rule(rule)
                self.grammar.remove_terminal(rule.right[0])
                continue

            updated_rule = rule.get_clone()
            if is_lhs_replaced:
                updated_rule.left = Z
            for index in rhs_index_list:
                updated_rule.right[index] = Z

            # Drop useless rule, Z -> Z
            if len(updated_rule.right) == 1 and updated_rule.left == updated_rule.right[0]: continue
            
            if is_lhs_replaced:
                Z.count = Z.count + updated_rule.count
            
            updated_rule.compute_hash()
            if updated_rule not in self.grammar.rule_dict[updated_rule.left]:
                self.grammar.insert_rule(updated_rule)
            else:
                existing_rule = self.grammar.name_to_rule[updated_rule.name]
                existing_rule.count = existing_rule.count + updated_rule.count
            self.grammar.delete_rule(rule)

        # Merge lexical rule
        if lexical_samples:
            merge_rule = self.grammar.add_lexical_rule(lexical_samples[0], Z)
            for sample in lexical_samples[1:]:
                merge_rule.right[0].add_sample(sample)
            merge_rule.left.count = merge_rule.left.count + lexical_rule_count
            merge_rule.count = lexical_rule_count

        # remove nonterminal X and Y
        self.grammar.remove_nonterminal(X)
        self.grammar.remove_nonterminal(Y)
        return 
    
    def chunk(self, symbol_list):
        nonterminal = self.grammar.add_nonterminal()
        
        # replaced_rule : [start_index_list]
        replaced_rule_dict = defaultdict(lambda: [])
        for rule, rhs_index in self.grammar.rhs_rule_dict[symbol_list[0]]:
            is_match = True
            for i, item in enumerate(symbol_list):
                if rhs_index+i >= len(rule.right) or item != rule.right[rhs_index + i]:
                    is_match = False
                    break
            if is_match:
                replaced_rule_dict[rule].append(rhs_index)

        new_rule_count = 0.0
        for rule, index_list in replaced_rule_dict.iteritems():
            updated_rule = rule.get_clone()
            index_list = sorted(index_list, reverse=True)
            for index in index_list:
                updated_rule.right = updated_rule.right[:index] + [nonterminal] + updated_rule.right[index+len(symbol_list):]

            new_rule_count = new_rule_count + updated_rule.count
            updated_rule.compute_hash()
            if updated_rule not in self.grammar.rule_dict[updated_rule.left]:
                self.grammar.insert_rule(updated_rule)
            else:
                existing_rule = self.grammar.name_to_rule[updated_rule.name]
                existing_rule.count = existing_rule.count + updated_rule.count
            self.grammar.delete_rule(rule)
                
        # add the new rule if needed
        if new_rule_count > 0:
            new_rule = Rule()
            new_rule.left = nonterminal 
            new_rule.right = symbol_list
            new_rule.count = new_rule_count
            new_rule.left.count = new_rule_count
            new_rule.compute_hash()
            self.grammar.insert_rule(new_rule)
        return 

    def train(self, initialize_number=4):
        
        tmp = 0
        
        self.grammar = FSCFG(features=self.features)
        self.grammar.start_symbol = self.grammar.add_nonterminal()
        self.grammar.feature_deviation = numpy.std(self.training_data, axis=(0,1))

        # Initialize grammar with naive rule
        # Becareful that self.grammar.epsilon_rule_probability is None -> Need to decorate it
        for sample_string in self.training_data[:initialize_number]:
            self.grammar.add_naive_rule(sample_string)
            
            tmp = tmp + 1
            print "\n\n\n***** Sample String %d *****\n\n\n" % (tmp)
            print sample_string
            self.grammar.show(0)
            
        self.grammar.compute_rule_probability()
        
        # Begin to parse the following training strings
        for sample_string in self.training_data[initialize_number:]:
            
            tmp = tmp + 1
            print "\n\n\n***** Sample String %d *****\n\n\n" % (tmp)
            
            # Use a grammar decorated with null productions and skip rules for all preterminals 
            self.grammar.add_skip_and_epsilon()
            #self.grammar.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in self.grammar.nonterminal_set}
            
            parser = Viterbi_Parser(self.grammar)
            parser.sample_string = sample_string
            parser.initialize_chart()
            recognize, probability = parser.parse()
            parser.show(0)
            print "\n*****recognize=%s probabilty=%g*****\n" % (recognize, probability)
            parser.show_parse_tree(parser.parse_tree, 0)
            if recognize and probability > self.parse_probability_threshold:
                # Trace the parse tree and update grammar parameters
                self.trace_parse_tree(parser.parse_tree, sample_string, [0,-1])
            else:
                # Add a naive rule for the string
                self.grammar.add_naive_rule([string])
            
            self.grammar.remove_skip_and_epsilon()
            self.grammar.compute_rule_probability()
            self.grammar.show(0)
        
        """
        parser = None
        self.grammar.nonterminal_to_order = None
        self.grammar.order_to_nonterminal = None
        self.grammar.rule_dict = None
        self.grammar.nonterminal_set = None
        self.grammar.name_to_symbol = None
        print sys.getrefcount(self.grammar.start_symbol)
        
        tmp2 = None
        for a in self.grammar.terminal_set:
            tmp2 = a
            break
        tmp2.show(0)
        parser = None
        self.grammar.name_to_symbol = None
        self.grammar.terminal_set = None
        self.grammar.rule_dict = None
        #self.grammar = None
        print sys.getrefcount(tmp2)
        """
        return 











