#!/usr/bin/python 
# -*- coding: utf-8 -*-

import re
import sys
import os 
import copy
import random
import numpy
import scipy
import scipy.stats
from collections import deque
from collections import defaultdict
from fscfg import Nonterminal, Terminal, Rule, FSCFG
from viterbi_parser import Viterbi_Parser
        
class FSCFG_Learner(object):
    def __init__(self, features, expected_rule_length=1, scan_tolerance=0.5, parse_probability_threshold=float("-inf")):
        self.grammar = None
        self.features = features
        
        # Store training data with 2 dim list indexed with [i][j], i: # strings to train, j: # samples in a string 
        self.training_data = []
        
        # Precompute distributions for speeding up
        self.rule_length_distribution = scipy.stats.poisson(expected_rule_length)
        self.scan_error = numpy.log(scipy.stats.norm(0,1).pdf(scan_tolerance)) * self.features
        return

    def read_training_data(self, input_path, normalize=True, max_size=1000):
        file_list = []
        for entry in os.listdir(input_path):
            entry_path = os.path.join(input_path, entry)
            if os.path.isfile(entry_path):
                file_list.append(entry_path)
        for file_path in sorted(file_list)[:max_size]:
            with open(file_path, "r") as f:
                sample_string = []
                for line in f.readlines():
                    string_list = line.strip().split(" ")
                    sample = [float(feature_value) for feature_value in string_list]
                    sample_string.append(sample)
                self.training_data.append(sample_string)
        
        # Normalize each feature to standard gaussion distribution
        if normalize:
            sample_list = numpy.array([sample for sample_string in self.training_data for sample in sample_string], dtype=float)
            sample_mean = numpy.mean(sample_list, axis=0)
            sample_deviation = numpy.std(sample_list, axis=0)
            for index, sample_string in enumerate(self.training_data):
                self.training_data[index] = (numpy.array(sample_string, dtype=float) - sample_mean) / sample_deviation
            self.feature_covariance = numpy.identity(self.features, dtype=float)
        else:
            sample_list = numpy.array([sample for sample_string in self.training_data for sample in sample_string], dtype=float)
            self.feature_covariance = numpy.diag(numpy.var(sample_list, axis=0))
            
        # Precompute distributions for speeding up
        #self.rule_probability_distribution = [None] + [scipy.stats.dirichlet([1.0/i]*i) for i in range(1, len(self.training_data)+1)]
        self.rule_count_probability = [None] + [scipy.stats.dirichlet([1.0]*i).pdf([1.0/i]*i) for i in range(1, len(self.training_data)+1)]
        return

    def trace_parse_tree(self, tree, sample_string, sample_index, leaf_terminal_set):
        symbol = tree[0][0]
        
        # Node: terminal
        if symbol in self.grammar.terminal_set:
            leaf_terminal_set.add(symbol)
            symbol.add_sample(sample_string[sample_index[0]])
            sample_index[0] = sample_index[0] + 1
            return
        
        rule = tree[0][1]
        
        # Node: nonterminal to epsilon
        if not rule: return
                
        # Node: nonterminal with normal rule or skip rule
        if rule not in self.grammar.skip_rule_set:
            # Normal rules
            rhs = []
            for child_tree in tree[1]:
                child_symbol = child_tree[0][0]
                if child_symbol in self.grammar.terminal_set or child_tree[0][1]:
                    rhs.append(child_symbol)
                    self.trace_parse_tree(child_tree, sample_string, sample_index, leaf_terminal_set)
                
            if len(rhs) == len(rule.right):
                rule.count = rule.count + 1.0
                rule.left.count = rule.left.count + 1.0
            else:
                # Some child nonterminals expand to epsilon
                modified_rule = rule.get_clone()
                modified_rule.right = rhs
                modified_rule.count = 1.0
                modified_rule.left.count = modified_rule.left.count + 1.0
                modified_rule.compute_hash()
                if modified_rule not in self.grammar.rule_dict[modified_rule.left]:
                    self.grammar.insert_rule(modified_rule)
                else:
                    existing_rule = self.grammar.name_to_rule[modified_rule.name]
                    existing_rule.count = existing_rule.count + modified_rule.count
        else:
            # N -> SKIP N or S -> S SKIP
            if rule.right[0] == self.grammar.skip_nonterminal:
                skip_rhs_index = 0
                skip_sample_index = sample_index[0]
                sample_index[0] = sample_index[0] + 1
            else:
                skip_rhs_index = 1
                skip_sample_index = sample_index[1]
                sample_index[1] = sample_index[1] - 1
            
            new_lexical_rule = self.grammar.add_lexical_rule(sample_string[skip_sample_index])
            modified_rule = rule.get_clone()
            modified_rule.right[skip_rhs_index] = new_lexical_rule.left
            modified_rule.count = 1.0
            modified_rule.left.count = modified_rule.left.count + 1.0
            
            child_tree = tree[1][1-skip_rhs_index]
            if child_tree[0][1]:
                self.trace_parse_tree(child_tree, sample_string, sample_index, leaf_terminal_set)
            else:
                # when child preterminal expand to epsilon
                modified_rule.right = [new_lexical_rule.left]
            modified_rule.compute_hash()
            self.grammar.insert_rule(modified_rule)
        return
    
    def train(self, initial_size=3, batch_size=10, history_size=10):
        self.grammar = FSCFG(features=self.features)
        self.grammar.start_symbol = self.grammar.add_nonterminal()
        
        # Initialize grammar with naive rules and optimize it
        for sample_string in self.training_data[:initial_size]:
            self.grammar.add_naive_rule(sample_string)
        self.grammar.compute_rule_probability()
        self.grammar.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in self.grammar.nonterminal_set}
        probability_list = self.best_first_search(self.training_data[:initial_size])
        print "***** Initial Grammar *****\n\n\n"
        self.grammar.show(0)
        
        # Use remaining sample strings batch by batch
        batch_index = initial_size
        while batch_index < len(self.training_data):
            print "\n\n\n***** Batch start at %d *****\n\n\n" % batch_index
            print self.training_data[batch_index:batch_index+batch_size]
            
            # First pass: update grammar when parse succeeds
            fail_index_list = []
            for string_index, sample_string in enumerate(self.training_data[batch_index:batch_index+batch_size]):
                self.grammar.add_skip_and_epsilon(self.scan_error)
                parser = Viterbi_Parser(self.grammar, scan_probability_threshold=self.scan_error, base_parse_threshold=numpy.percentile(probability_list, 50))
                parser.sample_string = sample_string
                parser.initialize_chart()
                recognize, probability = parser.parse()
                if recognize:
                    probability_list.append(probability)
                    leaf_terminal_set = set()
                    self.trace_parse_tree(parser.parse_tree, sample_string, [0,-1], leaf_terminal_set)
                    for terminal in leaf_terminal_set:
                        terminal.merge_samples()
                else:
                    fail_index_list.append(string_index)
                self.grammar.remove_skip_and_epsilon()
                self.grammar.compute_rule_probability()
            
            # Second pass: optimization based on failed strings, with some past strings to counter overfitting
            if fail_index_list:
                for string_index in fail_index_list:
                    self.grammar.add_naive_rule(self.training_data[string_index])
                self.grammar.compute_rule_probability()
                self.grammar.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in self.grammar.nonterminal_set}
                history_index_list = random.sample(range(batch_index), min(batch_index,history_size))
                probability_list = self.best_first_search([self.training_data[i] for i in (fail_index_list+history_index_list)])
                self.grammar.show(0)
            
            batch_index = batch_index + batch_size
            
        #self.grammar.add_skip_and_epsilon(self.scan_error)
        self.grammar.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in self.grammar.nonterminal_set}
        for sample_string in self.training_data:
            parser = Viterbi_Parser(self.grammar, scan_probability_threshold=self.scan_error, base_parse_threshold=float("-inf"))
            parser.sample_string = sample_string
            parser.initialize_chart()
            recognize, probability = parser.parse()
            print sample_string
            print "recognize=%s probability=%g" % (recognize, probability)
            if recognize: parser.show_parse_tree(parser.parse_tree, 1)
        #self.grammar.remove_skip_and_epsilon()
        return 

    def get_grammar_usage_of_parse_tree(self, tree, sample_index, terminal_usage, rule_usage, nonterminal_usage):
        symbol = tree[0][0]
        rule = tree[0][1]
        child_list = tree[1]
        
        if symbol in self.grammar.terminal_set:
            terminal_usage[symbol].add(sample_index[0])
            sample_index[0] = sample_index[0] + 1
        else:
            rule_usage[rule] = rule_usage[rule] + 1
            nonterminal_usage.add(rule.left)
            for child in child_list:
                self.get_grammar_usage_of_parse_tree(child, sample_index, terminal_usage, rule_usage, nonterminal_usage)
        return
    
    def best_first_search(self, training_data):
        while True:
            # Evaluate grammar and store usage
            probability_list = [float("-inf") for i in range(len(training_data))]
            terminal_usage_list = [defaultdict(lambda: set()) for i in range(len(training_data))]
            rule_usage_list = [defaultdict(lambda: 0) for i in range(len(training_data))]
            nonterminal_usage = set()
            
            for string_index, sample_string in enumerate(training_data):
                parser = Viterbi_Parser(self.grammar, scan_probability_threshold=self.scan_error)
                parser.sample_string = sample_string
                parser.initialize_chart()
                recognize, probability_list[string_index] = parser.parse()
                if not recognize:
                    print sample_string
                    raise AssertionError("The above string is not recognized by current grammar")
                self.get_grammar_usage_of_parse_tree(parser.parse_tree, [0], terminal_usage_list[string_index], rule_usage_list[string_index], nonterminal_usage)
            
            min_cost = self.evaluate_grammar(self.grammar, probability_list)
            better_grammar = None
            
            # Store action as ["merge"/"chunk", new_nonterminal, merged_terminal, old_nonterminal_list]
            action = [None, None, None, []]
            
            # Search all merge actions
            for N_i in self.grammar.nonterminal_set:
                nonterminal_usage = nonterminal_usage - set([N_i])
                for N_j in nonterminal_usage:
                    current_grammar = self.grammar.get_clone()
                    N_z, affected_terminal_dict, affected_rule_dict = current_grammar.merge(self.grammar.name_to_symbol, N_i, N_j)
                    current_grammar.compute_rule_probability()
                    current_grammar.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in current_grammar.nonterminal_set}
                    affected_probability_list = self.get_affected_probablility(training_data, probability_list, terminal_usage_list, rule_usage_list, affected_terminal_dict, affected_rule_dict)
                    current_cost = self.evaluate_grammar(current_grammar, affected_probability_list)
                    if min_cost > current_cost:
                        better_grammar = current_grammar
                        min_cost = current_cost
                        action = ["merge", N_z, [N_i, N_j]]
                        
            # Search all chunk actions
            for nonterminal in self.grammar.nonterminal_set:
                for rule in self.grammar.rule_dict[nonterminal]:
                    for rhs_i in range(len(rule.right)-1):
                        for rhs_j in range(rhs_i+1, len(rule.right)):
                            current_grammar = self.grammar.get_clone()
                            symbol_string = [current_grammar.name_to_symbol[symbol.name] for symbol in rule.right[rhs_i:rhs_j+1]]
                            N_z, chunks, affected_rule_dict = current_grammar.chunk(symbol_string)
                            current_grammar.compute_rule_probability()
                            current_grammar.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in current_grammar.nonterminal_set}
                            if chunks < 2: continue
                            affected_probability_list = self.get_affected_probablility(training_data, probability_list, None, rule_usage_list, {}, affected_rule_dict)
                            current_cost = self.evaluate_grammar(current_grammar, affected_probability_list)
                            if min_cost > current_cost:
                                better_grammar = current_grammar
                                min_cost = current_cost
                                action = ["chunk", N_z, symbol_string]
                                
            if not better_grammar: break
            
            # Only merge samples once per iteration
            if affected_terminal_dict:
                affected_terminal_dict.values()[0].merge_samples()
            self.grammar = better_grammar
            
            print action[0]
            action[1].show(1)
            for symbol in action[2]:
                symbol.show(2) 
            
        return probability_list

    def get_affected_probablility(self, training_data, probability_list, terminal_usage_list, rule_usage_list, affected_terminal_dict, affected_rule_dict):
        affected_probability_list = probability_list[:]
        for string_index, sample_string in enumerate(training_data):
            for affected_terminal, new_terminal in affected_terminal_dict.iteritems():
                if affected_terminal not in terminal_usage_list[string_index]: continue
                for sample_index in terminal_usage_list[string_index][affected_terminal]:
                    # TODO: store scan probability in scanner
                    old_scan_probability = affected_terminal.scan(sample_string[sample_index], self.feature_covariance)
                    new_scan_probability = new_terminal.scan(sample_string[sample_index], self.feature_covariance)
                    affected_probability_list[string_index] = affected_probability_list[string_index] - old_scan_probability + new_scan_probability
            for affected_rule, new_rule in affected_rule_dict.iteritems():
                if affected_rule not in rule_usage_list[string_index]: continue
                affected_probability_list[string_index] = affected_probability_list[string_index] + (new_rule.probability - affected_rule.probability) * rule_usage_list[string_index][affected_rule]
        return affected_probability_list
        
    def evaluate_grammar(self, grammar, probability_list, prior_weight=1):
        discription_length = grammar.get_discription_length(self.rule_length_distribution, self.rule_count_probability)
        cost = discription_length * prior_weight - numpy.log(numpy.mean(numpy.exp(probability_list)))
        return cost











