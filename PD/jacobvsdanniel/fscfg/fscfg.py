#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import sys
import copy
import numpy
import scipy
import scipy.stats
from collections import defaultdict
from sklearn.cluster import KMeans

class Nonterminal(object):
    def __init__(self, name):
        self.name = name
        # Record use count in training data for FSCFG learner
        self.count = 0.0
        return

    def show(self, indent):
        print "    "*indent + "[%s] cnt=%g" % (self.name, self.count)

class Terminal(object):
    def __init__(self, name, features, sample_list):
        self.name = name
        self.features = features
        self.feature_weight = 0.0
        self.sample_dict = defaultdict(lambda: 0.0)
        self.samples = 0.0
        self.sample_sum = numpy.zeros(self.features, dtype=float)
        self.sample_square_sum = numpy.zeros(self.features, dtype=float)
        
        # Records for fast calculation of standard deviation of feature vector
        self.sample_sum = numpy.zeros(self.features, dtype=float)
        self.sample_square_sum = numpy.zeros(self.features, dtype=float)
        
        for sample in sample_list:
            self.add_sample(sample)
        """
        if sample_list:
            self.merge_samples()
        """
        return
        
    def add_sample(self, sample, sample_to_terminal=None):
        if len(self.sample_dict) == 1:
            single_peak = True
            peak = next(iter(self.sample_dict))
        else:
            single_peak = False
    
        # Record to calculate standard deviation of feature vector faster
        sample = tuple(sample)
        self.sample_dict[sample] = self.sample_dict[sample] + 1.0
        self.samples = self.samples + 1.0
        self.sample_sum = self.sample_sum + numpy.array(sample)
        self.sample_square_sum = self.sample_square_sum + numpy.array(sample)**2
        
        mean = self.sample_sum / self.samples
        variance = (self.sample_square_sum - 2*mean*self.sample_sum) / self.samples + mean**2
        deviation = numpy.sqrt(numpy.where(variance<0, 0.0, variance))
        self.feature_weight = numpy.log(numpy.prod(1.0 / (deviation+1.0)))
        
        if sample_to_terminal and single_peak and len(self.sample_dict)>1:
            del sample_to_terminal[peak]
        return
 
    def merge_terminal(self, other, sample_to_terminal):
        if len(self.sample_dict) == 1:
            single_peak = True
            peak = next(iter(self.sample_dict))
        else:
            single_peak = False
        
        for sample in other.sample_dict.keys():
            self.sample_dict[sample] = self.sample_dict[sample] + other.sample_dict[sample]
        self.samples = self.samples + other.samples
        self.sample_sum = self.sample_sum + other.sample_sum
        self.sample_square_sum = self.sample_square_sum + other.sample_square_sum
        
        mean = self.sample_sum / self.samples
        variance = (self.sample_square_sum - 2*mean*self.sample_sum) / self.samples + mean**2
        deviation = numpy.sqrt(numpy.where(variance<0, 0.0, variance))
        self.feature_weight = numpy.log(numpy.prod(1.0 / (deviation+1.0)))
        
        if single_peak and len(self.sample_dict)>1:
            del sample_to_terminal[peak]
        return
        """
        # Merge samples of each other, and merge similar samples
        for sample in other.sample_dict.keys():
            self.sample_dict[sample] = self.sample_dict[sample] + other.sample_dict[sample]
        self.merge_samples()
        """
    """
    def merge_samples(self, max_clusters=100, improvement_threshold=0.001):
        # Apply k-means to merge similar samples
        
        # Copy sample dict to sample list for kmeans clustering
        sample_list = []
        for sample, weight in self.sample_dict.iteritems():
            sample_list.extend([sample] * int(weight))

        # Find the best number of clusters
        best_kmeans = None
        improvement_threshold = improvement_threshold * self.features * len(sample_list)
        #print "threshold=%g" % improvement_threshold
        max_clusters = min(max_clusters, len(self.sample_dict))
        for clusters in range(1, max_clusters+1):
            kmeans = KMeans(n_clusters=clusters)
            kmeans.fit(sample_list)
            #print kmeans.inertia_
            if best_kmeans and best_kmeans.inertia_ - kmeans.inertia_ < improvement_threshold: break
            best_kmeans = kmeans
            
        # Use clustered centers as samples
        self.feature_weight = 0.0
        self.sample_dict = defaultdict(lambda: 0.0)
        self.samples = 0.0
        self.sample_sum = numpy.zeros(self.features, dtype=float)
        self.sample_square_sum = numpy.zeros(self.features, dtype=float)
        
        if best_kmeans:
            for label in best_kmeans.labels_:
                self.add_sample(best_kmeans.cluster_centers_[label])
        return
    """
    def scan(self, input_sample, feature_covariance=None):
        # skip_terminal
        if self.samples == 0: return 0
        
        if feature_covariance is None:
            feature_covariance = numpy.identity(self.features) / 10.0
        
        probability_list = []
        for sample, sample_weight in self.sample_dict.iteritems():
            # TODO: bottleneck
            probability = scipy.stats.multivariate_normal.logpdf(input_sample, mean=sample, cov=feature_covariance, allow_singular=False)
            probability_list.append(numpy.log(sample_weight) + probability)

        probability_list = numpy.array(probability_list)
        probability_max = numpy.max(probability_list)
            
        offset = 0
        if probability_max < -600:
            offset = 600 - probability_max
            probability_list = probability_list + offset
            
        return numpy.log(numpy.sum(numpy.exp(probability_list))) - offset + self.feature_weight - numpy.log(self.samples)
    
    def show(self, indent):
        print "    "*indent + "[%s] weight=%g" % (self.name, self.feature_weight)
        # samples
        for sample, count in self.sample_dict.iteritems():
            output = "    "*(indent+1)
            for feature_value in sample:
                output = output + " % .2f" % feature_value
            output = output + " [%g]" % count
            print output

class Rule(object):
    def __init__(self):
        self.name = None
        self.left = None
        self.right = []
        self.probability = None
        self.hash_value = None
        # Record used count in training data for FSCFG learner
        self.count = 0.0
        return
    
    def get_clone(self, name_to_symbol=None):
        clone = Rule()
        if name_to_symbol:
            clone.left = name_to_symbol[self.left.name]
            clone.right = [name_to_symbol[symbol.name] for symbol in self.right]
        else:
            clone.left = self.left
            clone.right = self.right[:]
        clone.probability = self.probability
        clone.count = self.count
        return clone
    
    def read_rule_line(self, line, name_to_symbol):
        string_list = line.strip().split(" ")
        self.name = string_list[0]
        self.left = name_to_symbol[string_list[1]]
        self.right = [name_to_symbol[name] for name in string_list[2:-1]]
        self.probability = numpy.log(float(string_list[-1]))
        return
        
    def compute_hash(self):
        self.name = "".join([self.left.name]+[symbol.name for symbol in self.right])
        self.hash_value = hash(self.name)
        
    def __eq__(self, other):
        return self.name == other.name
        
    def __ne__(self, other):
        return not(self == other)
        
    def __hash__(self):
        return self.hash_value
    
    def show_short(self, indent):
        output = "    "*indent + "%s -> " % self.left.name
        output = output + " ".join([symbol.name for symbol in self.right])
        print output
        return
    
    def show(self, indent):
        output = "    "*indent + "[% 4d % 7.2f ] %s -> " % (self.count, self.probability, self.left.name)
        output = output + " ".join([symbol.name for symbol in self.right])
        print output
        return

class FSCFG(object):
    def __init__(self, features):
        self.terminal_set = set()
        self.nonterminal_set = set()
        self.start_symbol = None
        self.rule_dict = {}
        self.rhs_rule_dict = {}
        self.features = features
        self.feature_covariance = numpy.identity(self.features, dtype=float) / 10.0
        
        # Store next-to-use name suffixes
        self.nonterminal_index = 0
        self.terminal_index = 0
        
        # A 2D dict representing probabilistic reflexive and transitive left corner relation
        self.left_relation = None
        
        # A 2D dict representing probabilistic reflexive and transitive unit production relation
        self.unit_relation = None

        # A dict storing epsilon probabilities
        self.epsilon_rule_probability = None
        self.epsilon_expansion_probability = None

        # Map symbol name to reference
        self.name_to_symbol = {}
        
        # Map rule name to reference
        self.name_to_rule = {}
        
        # Map sample to single-peak terminal
        self.sample_to_terminal = {}
        return
    
    def get_clone(self):
        clone = FSCFG(self.features)
        clone.terminal_set = copy.deepcopy(self.terminal_set)
        clone.nonterminal_set = copy.deepcopy(self.nonterminal_set)
        
        for symbol in clone.terminal_set:
            clone.name_to_symbol[symbol.name] = symbol
        for symbol in clone.nonterminal_set:
            clone.name_to_symbol[symbol.name] = symbol
        for sample, terminal in self.sample_to_terminal.iteritems():
            clone.sample_to_terminal[sample] = clone.name_to_symbol[terminal.name]
        
        clone.start_symbol = clone.name_to_symbol[self.start_symbol.name]
        clone.nonterminal_index = self.nonterminal_index
        clone.terminal_index = self.terminal_index
        
        for nonterminal, rule_set in self.rule_dict.iteritems():
            clone_nonterminal = clone.name_to_symbol[nonterminal.name]
            clone.rule_dict[clone_nonterminal] = set()
            for rule in rule_set:
                clone_rule = rule.get_clone(clone.name_to_symbol)
                clone_rule.compute_hash()
                clone.rule_dict[clone_rule.left].add(clone_rule)
                clone.name_to_rule[clone_rule.name] = clone_rule
                
        for nonterminal, rule_index_set in self.rhs_rule_dict.iteritems():
            clone_nonterminal = clone.name_to_symbol[nonterminal.name]
            clone.rhs_rule_dict[clone_nonterminal] = set([(clone.name_to_rule[rule.name], index) for rule, index in rule_index_set])
        
        clone.feature_covariance = numpy.copy(self.feature_covariance)
        return clone
        
    def read_terminal_file(self, input_file):
        self.terminal_set.clear()
        with open(input_file, "r") as f:
            for line in f.readlines():
                part_list = line.strip().split("|")
                sample_list = []
                for part in part_list[1:]:
                    sample = [float(feature_value) for feature_value in part.strip().split(" ")]
                    if len(sample) != self.features:
                        raise AssertionError("Unexpected sample length: %d" % len(sample))
                    sample_list.append(sample)
                terminal = Terminal(part_list[0].strip(), self.features, sample_list)
                self.terminal_set.add(terminal)
                self.name_to_symbol[terminal.name] = terminal
        return

    def read_nonterminal_file(self, input_file):
        self.start_symbol = None
        self.nonterminal_set.clear()
        index = -1
        with open(input_file, "r") as f:
            for line in f.readlines():
                index = index + 1
                nonterminal = Nonterminal(line.strip())
                self.nonterminal_set.add(nonterminal)
                self.name_to_symbol[nonterminal.name] = nonterminal
                # Set the first as start symbol
                if not self.start_symbol:
                    self.start_symbol = nonterminal
        return

    def read_rule_file(self, input_file):
        """ Not storing epsilon rules in rule_dict """
        self.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in self.nonterminal_set}
        self.rule_dict.clear()
        self.rhs_rule_dict.clear()
        self.name_to_rule.clear()
        rule_dict = {nonterminal: set() for nonterminal in self.nonterminal_set}
        rhs_rule_dict = {nonterminal: set() for nonterminal in self.nonterminal_set}
        rhs_rule_dict.update({terminal: set() for terminal in self.terminal_set})
        name_to_rule = {}

        with open(input_file, "r") as f:
            for line in f.readlines():
                rule = Rule()
                rule.read_rule_line(line, self.name_to_symbol)
                rule.compute_hash()
        
                if not rule.right:
                    self.epsilon_rule_probability[rule.left] = rule.probability
                else:
                    rule_dict[rule.left].add(rule)
                    for rhs_index, symbol in enumerate(rule.right):
                        rhs_rule_dict[symbol].add((rule, rhs_index))
                    name_to_rule[rule.name] = rule
                
        for nonterminal, rule_set in rule_dict.iteritems():
            total_probability = sum([numpy.exp(rule.probability) for rule in rule_set])
            total_probability = total_probability + numpy.exp(self.epsilon_rule_probability[nonterminal])
            if not numpy.isclose([total_probability], [1]):
                raise AssertionError("Total probability for LHS [%s] is %g, not unity" % (nonterminal.name, total_probability))

        self.rule_dict = rule_dict
        self.rhs_rule_dict = rhs_rule_dict
        self.name_to_rule = name_to_rule
        return
    
    def add_nonterminal(self):
        """ Add a nonterminal and return its reference """
        nonterminal = Nonterminal("N%03d" % self.nonterminal_index)
        self.nonterminal_index = self.nonterminal_index + 1
        self.nonterminal_set.add(nonterminal)
        
        self.name_to_symbol[nonterminal.name] = nonterminal
        self.rule_dict[nonterminal] = set()
        self.rhs_rule_dict[nonterminal] = set()
        return nonterminal
        
    def remove_nonterminal(self, nonterminal):
        self.nonterminal_set.remove(nonterminal)
        del self.name_to_symbol[nonterminal.name]
        del self.rule_dict[nonterminal]
        del self.rhs_rule_dict[nonterminal]
        return
        
    def add_terminal(self, sample_list):
        """ Add a terminal and return its reference """
        terminal = Terminal("t%d" % self.terminal_index, self.features, sample_list)
        self.terminal_index = self.terminal_index + 1
        self.terminal_set.add(terminal)
        self.name_to_symbol[terminal.name] = terminal
        self.rhs_rule_dict[terminal] = set()
        return terminal
    
    def remove_terminal(self, terminal):
        self.terminal_set.remove(terminal)
        del self.name_to_symbol[terminal.name]
        del self.rhs_rule_dict[terminal]
        if len(terminal.sample_dict) == 1:
            peak = next(iter(terminal.sample_dict))
            del self.sample_to_terminal[peak]
        return
    
    def insert_rule(self, rule):
        self.rule_dict[rule.left].add(rule)
        for rhs_index, symbol in enumerate(rule.right):
            self.rhs_rule_dict[symbol].add((rule, rhs_index))
        self.name_to_rule[rule.name] = rule
        return
    
    def delete_rule(self, rule):
        self.rule_dict[rule.left].discard(rule)
        for rhs_index, symbol in enumerate(rule.right):
            self.rhs_rule_dict[symbol].discard((rule, rhs_index))
        del self.name_to_rule[rule.name]
        return
    
    def add_lexical_rule(self, sample):
        """ Add the lexical rule N_i -> t_j [1] with only one sample for t_j """
        
        # TODO: iterate through sample_to_terminal with isclose()?
        sample = tuple(sample)
        if sample in self.sample_to_terminal:
            terminal = self.sample_to_terminal[sample]
            terminal.add_sample(sample)
            rule = next(iter(self.rhs_rule_dict[terminal]))[0]
            rule.left.count = rule.left.count + 1
            rule.count = rule.count + 1
            return rule
        else:
            terminal = self.add_terminal([sample])
            self.sample_to_terminal[sample] = terminal
        
        rule = Rule()
        rule.left = self.add_nonterminal()
        rule.left.count = 1.0
        rule.right = [terminal]
        rule.probability = 0.0
        rule.count = 1.0
        rule.compute_hash()
        self.rule_dict[rule.left].add(rule)
        self.rhs_rule_dict[rule.right[0]].add((rule,0))
        self.name_to_rule[rule.name] = rule
        return rule
    
    def add_naive_rule(self, sample_string):
        """ Add the naive rule S -> N_1 N_2 ... N_m [1] """
        rhs = []
        for sample in sample_string:
            lexical_rule = self.add_lexical_rule(sample)
            rhs.append(lexical_rule.left)

        rule = Rule()
        rule.left = self.start_symbol
        rule.right = rhs
        rule.probability = 0.0
        rule.left.count = rule.left.count + 1.0
        rule.count = 1.0
        rule.compute_hash()
        self.insert_rule(rule)
        return rule
    
    def get_preterminal_set(self):
        preterminal_set = set()
        for terminal in self.terminal_set:
            try:
                preterminal_set.add(next(iter(self.rhs_rule_dict[terminal]))[0].left)
            except StopIteration:
                pass
        return preterminal_set
    
    def compute_insertion_error(self, input_length, scan_error):
        
        # Compute expected rule length
        #rule_length = sum([len(rule.right) for rule in self.grammar.name_to_rule.values()]) / float(len(self.grammar.nonterminal_set))
        rule_length = 2
        
        # Compute expected rule probability of variables (non-preterminal nonterminal)
        variable_set = self.nonterminal_set - self.get_preterminal_set()
        variables = float(len(variable_set))
        rules = sum([len(self.rule_dict[variable]) for variable in variable_set])
        rule_probability = numpy.log(variables/rules)
        
        return numpy.log(input_length) / numpy.log(rule_length) * rule_probability + scan_error
    
    def add_skip_and_epsilon(self, input_length, scan_error=None):
        """ Decorations which will only be added right before parser initialization and will be removed right after tracing parse tree """
        self.skip_nonterminal = self.add_nonterminal()
        self.skip_terminal = self.add_terminal([])
        self.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in self.nonterminal_set}
        
        if not scan_error:
            scan_error = scipy.stats.multivariate_normal.logpdf([2.0]*self.features, mean=[0.0]*self.features, cov=numpy.identity(self.features))
        insertion_error = self.compute_insertion_error(input_length, scan_error)
        #sys.stderr.write("%g" % insertion_error)
        
        # Bookkeeping for latter deletion
        self.skip_rule_set = set()
        
        # Add rule: START -> START SKIP
        skip_rule = Rule()
        skip_rule.left = self.start_symbol
        skip_rule.right = [self.start_symbol, self.skip_nonterminal]
        skip_rule.probability = insertion_error
        skip_rule.compute_hash()
        self.rule_dict[skip_rule.left].add(skip_rule)
        self.skip_rule_set.add(skip_rule)
        
        # Add rules: N_preterminal -> SKIP N_preterminal
        # Add null productions for N_preterminals
        for terminal in self.terminal_set:
            try:
                preterminal = next(iter(self.rhs_rule_dict[terminal]))[0].left
            except StopIteration:
                continue
            skip_rule = Rule()
            skip_rule.left = preterminal
            skip_rule.right = [self.skip_nonterminal, preterminal]
            skip_rule.probability = insertion_error
            skip_rule.compute_hash()
            self.rule_dict[skip_rule.left].add(skip_rule)
            self.skip_rule_set.add(skip_rule)
            self.epsilon_rule_probability[preterminal] = scan_error
        
        # Add rule: SKIP -> skip
        skip_rule = Rule()
        skip_rule.left = self.skip_nonterminal
        skip_rule.right = [self.skip_terminal]
        skip_rule.probability = 0.0
        skip_rule.compute_hash()
        self.rule_dict[skip_rule.left].add(skip_rule)
        return
    
    def remove_skip_and_epsilon(self):
        self.remove_nonterminal(self.skip_nonterminal)
        self.skip_nonterminal = None
        self.remove_terminal(self.skip_terminal)
        self.skip_terminal = None
        self.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in self.nonterminal_set}
        for rule in self.skip_rule_set:
            self.rule_dict[rule.left].remove(rule)
        
    def compute_rule_probability(self):
        for nonterminal, rule_set in self.rule_dict.iteritems():
            for rule in rule_set:
                rule.probability = numpy.log(rule.count/nonterminal.count)
        return
    
    def compute_left_relation(self):
        # Get nonterminal to order mapping:
        nonterminal_to_order = {}
        order_to_nonterminal = {}
        for order, nonterminal in enumerate(self.nonterminal_set):
            nonterminal_to_order[nonterminal] = order
        
        # Initialize both relations
        nonterminals = len(self.nonterminal_set)
        left_relation = numpy.zeros((nonterminals,nonterminals), dtype=float)
        #unit_relation = numpy.zeros((nonterminals,nonterminals), dtype=float)
        
        # Compute probabilities of non-transitive relations
        for nonterminal in self.nonterminal_set:
            row = nonterminal_to_order[nonterminal]
            for rule in self.rule_dict[nonterminal]:
                probability = numpy.exp(rule.probability)
                for symbol in rule.right:
                    if symbol not in self.nonterminal_set: break
                    column = nonterminal_to_order[symbol]
                    left_relation[row][column] = left_relation[row][column] + probability
                    if self.epsilon_expansion_probability[symbol] == float("-inf"): break
                    probability = probability * numpy.exp(self.epsilon_expansion_probability[symbol])
                """
                if rule.right[0] in self.nonterminal_set:
                    row = nonterminal_to_order[nonterminal]
                    column = nonterminal_to_order[rule.right[0]]
                    left_relation[row][column] = left_relation[row][column] + numpy.exp(rule.probability)
                    #if len(rule.right) == 1: unit_relation[row][column] = unit_relation[row][column] + numpy.exp(rule.probability)
                """
        # Compute reflexive and transitive relation
        identity_relation = numpy.identity(nonterminals)
        with numpy.errstate(divide='ignore', invalid='ignore'):
            left_relation = numpy.log(numpy.linalg.inv(identity_relation - left_relation))
        left_relation = numpy.where(numpy.isnan(left_relation), float("-inf"), left_relation)
        #unit_relation = numpy.log(numpy.linalg.inv(identity_relation - unit_relation))
        
        # Transform to dense array
        self.left_relation = {}
        #self.unit_relation = {}
        for N_i in self.nonterminal_set:
            self.left_relation[N_i] = {}
            #self.unit_relation[N_i] = {}
            row = nonterminal_to_order[N_i]
            for N_j in self.nonterminal_set:
                column = nonterminal_to_order[N_j]
                if left_relation[row][column] > float("-inf"):
                    self.left_relation[N_i][N_j] = left_relation[row][column]
                #if unit_relation[N_i][N_j] > float("-inf"): self.unit_relation[N_i][N_j] = unit_relation[row][column]
        return
    
    def compute_epsilon_expansion(self, improvement_bound=1e-10):
        self.epsilon_expansion_probability = copy.copy(self.epsilon_rule_probability)
        
        # Get a rule_dict of pure nonterminal rules
        rule_dict = {nonterminal: set() for nonterminal in self.nonterminal_set}
        for nonterminal, rule_set in self.rule_dict.iteritems():
            for rule in rule_set:
                has_terminal = False
                for symbol in rule.right:
                    if symbol in self.terminal_set:
                        has_terminal = True
                        break
                if not has_terminal:
                    rule_dict[nonterminal].add(rule)
                    
        # Compute epsilon expansion
        while True:
            converge = True
            for nonterminal in self.nonterminal_set:
                update_value = self.epsilon_rule_probability[nonterminal]
                for rule in rule_dict[nonterminal]:
                    rhs_epsilon_probability = sum([self.epsilon_expansion_probability[symbol] for symbol in rule.right])
                    update_value = max(update_value, rule.probability+rhs_epsilon_probability)
                if update_value!=float("-inf") and numpy.exp(update_value) - numpy.exp(self.epsilon_expansion_probability[nonterminal]) > improvement_bound:
                    converge = False
                self.epsilon_expansion_probability[nonterminal] = update_value
            if converge: break
        """
        print "epsilon expansion probability:"
        output = "    "
        for nonterminal, probability in self.epsilon_expansion_probability.iteritems():
            output = output + "%s=%g " % (nonterminal.name, probability) 
        print output
        """
        return
    
    def get_discription_length(self, rule_length_distribution=None, rule_count_probability=[None]):
        """ Get negative logarithm of grammar prior """
        discription_length = 0.0
        
        nonterminal_bits = numpy.log(len(self.nonterminal_set))
        terminal_bits = numpy.log(len(self.terminal_set))
        
        for nonterminal, rule_set in self.rule_dict.iteritems():
            # Add -log of rule distribution prior
            rules = len(rule_set)
            if rules<len(rule_count_probability) and rule_count_probability[rules]:
                discription_length = discription_length - numpy.log(rule_count_probability[rules])
            else:
                #rule_distribution = numpy.exp([rule.probability for rule in rule_set])
                discription_length = discription_length - numpy.log(scipy.stats.dirichlet([1.0]*rules).pdf([1.0/rules]*rules))
            
            for rule in rule_set:
                # Add rule encoding length 
                if rule.right[0] in self.terminal_set:
                    discription_length = discription_length + terminal_bits
                else:
                    discription_length = discription_length + nonterminal_bits * len(rule.right)
                
                # Add -log of rule length prior
                # TODO: tune expected_rule_length
                if not rule_length_distribution:
                    rule_length_distribution = scipy.stats.poisson(1)
                discription_length = discription_length - numpy.log(rule_length_distribution.pmf(len(rule.right)-1))
                
        return discription_length

    def merge(self, name_to_symbol, symbol_X, symbol_Y):
        """ Given nonterminal X and Y, generate new nonterminal Z = merge(X, Y), return is merge successful """
        merge_nonterminal = [self.name_to_symbol[symbol_X.name], self.name_to_symbol[symbol_Y.name]]
        Z = self.add_nonterminal()

        # Store affected terminals and rules for fast grammar evaluation
        affected_terminal_dict = {}
        affected_rule_dict = {}

        # If X or Y is start symbol, new nonterminal should be the start symbol
        if self.start_symbol == merge_nonterminal[0] or self.start_symbol == merge_nonterminal[1]:
            self.start_symbol = Z

        # {to_be_replaced_rule: [is_lhs_replaced, rhs_index_list]}
        replaced_rule_dict = defaultdict(lambda: [False, []])
        for symbol in merge_nonterminal:
            for rule in self.rule_dict[symbol]:
                replaced_rule_dict[rule][0] = True
            for rule, rhs_index in self.rhs_rule_dict[symbol]:
                replaced_rule_dict[rule][1].append(rhs_index)
       
        lexical_rule_list = []
        for rule, (is_lhs_replaced, rhs_index_list) in replaced_rule_dict.iteritems():
            # Handle lexical rule
            if rule.right[0] in self.terminal_set:
                lexical_rule_list.append(rule)
                continue
            updated_rule = rule.get_clone()
            if is_lhs_replaced:
                updated_rule.left = Z
            for index in rhs_index_list:
                updated_rule.right[index] = Z

            # Drop useless rule, Z -> Z
            if len(updated_rule.right) == 1 and updated_rule.left == updated_rule.right[0]: continue
            if is_lhs_replaced:
                Z.count = Z.count + updated_rule.count
            
            updated_rule.compute_hash()
            if updated_rule not in self.rule_dict[updated_rule.left]:
                self.insert_rule(updated_rule)
                affected_rule_dict[rule] = updated_rule # store affected rule mapping its new rule
            else:
                existing_rule = self.name_to_rule[updated_rule.name]
                existing_rule.count = existing_rule.count + updated_rule.count
                affected_rule_dict[rule] = existing_rule # store affected rule mapping its new rule
            self.delete_rule(rule)

        # Merge lexical rule
        if len(lexical_rule_list) == 1:
            lexical_rule = lexical_rule_list[0].get_clone()
            lexical_rule.left = Z
            lexical_rule.left.count = lexical_rule.left.count + lexical_rule.count
            lexical_rule.compute_hash()
            affected_rule_dict[lexical_rule_list[0]] = lexical_rule # store affected rule mapping its new rule
            self.delete_rule(lexical_rule_list[0])
            self.insert_rule(lexical_rule)
        elif len(lexical_rule_list) == 2:
            lexical_rule_count = sum([rule.count for rule in lexical_rule_list])
            if len(lexical_rule_list[0].right[0].sample_dict) < len(lexical_rule_list[1].right[0].sample_dict): index = 1
            else: index = 0
            lexical_rule = lexical_rule_list[index].get_clone()
            lexical_rule.left = Z
            lexical_rule_list[index].right[0].merge_terminal(lexical_rule_list[1-index].right[0], self.sample_to_terminal)
            lexical_rule.left.count = lexical_rule.left.count + lexical_rule_count
            lexical_rule.count = lexical_rule_count
            lexical_rule.compute_hash()
            for rule_index in range(2):
                affected_rule_dict[lexical_rule_list[rule_index]] = lexical_rule # store affected rule mapping its new rule
                affected_terminal_dict[name_to_symbol[lexical_rule_list[rule_index].right[0].name]] = lexical_rule.right[0] # store affected terminal mapping to its new terminal
                self.delete_rule(lexical_rule_list[rule_index])
            self.remove_terminal(lexical_rule_list[1-index].right[0])
            self.insert_rule(lexical_rule)

        # remove nonterminal X and Y
        self.remove_nonterminal(merge_nonterminal[0])
        self.remove_nonterminal(merge_nonterminal[1])
        return Z, affected_terminal_dict, affected_rule_dict
    
    def chunk(self, symbol_list):
        """ Given nonterminal string [X1, X2, ...], nonterminal = chunk([X1, X2, ...]), return number of chunk positions """
        nonterminal = self.add_nonterminal()

        # Store affected rules for fast grammar evaluation
        affected_rule_dict = {}
        
        # replaced_rule : [start_index_list]
        replaced_rule_dict = defaultdict(lambda: [])
        for rule, rhs_index in self.rhs_rule_dict[symbol_list[0]]:
            is_match = True
            for i, item in enumerate(symbol_list):
                if rhs_index+i >= len(rule.right) or item != rule.right[rhs_index + i]:
                    is_match = False
                    break
            if is_match:
                replaced_rule_dict[rule].append(rhs_index)

        new_rule_count, num_of_chunk= 0.0, 0.0
        for rule, index_list in replaced_rule_dict.iteritems():
            updated_rule = rule.get_clone()
            index_list = sorted(index_list, reverse=True)
            last_update_start = len(updated_rule.right) + len(symbol_list)
            for index in index_list:
                if index+len(symbol_list) > last_update_start: continue
                updated_rule.right = updated_rule.right[:index] + [nonterminal] + updated_rule.right[index+len(symbol_list):]
                last_update_start = index
                num_of_chunk = num_of_chunk + 1.0

            new_rule_count = new_rule_count + updated_rule.count
            updated_rule.compute_hash()
            if updated_rule not in self.rule_dict[updated_rule.left]:
                self.insert_rule(updated_rule)
                affected_rule_dict[rule] = updated_rule # store affected rule mapping its new rule
            else:
                existing_rule = self.name_to_rule[updated_rule.name]
                existing_rule.count = existing_rule.count + updated_rule.count
                affected_rule_dict[rule] = existing_rule # store affected rule mapping its new rule
            self.delete_rule(rule)
                
        # add the new rule if needed
        if new_rule_count > 0:
            new_rule = Rule()
            new_rule.left = nonterminal 
            new_rule.right = symbol_list
            new_rule.count = new_rule_count
            new_rule.left.count = new_rule_count
            new_rule.compute_hash()
            self.insert_rule(new_rule)
        return nonterminal, num_of_chunk, affected_rule_dict
   
    def show(self, indent):
        print "    "*indent + "FSCFG:"
        """
        print "    "*(indent+1) + "feature deviation array:"
        print "    "*(indent+2) + " ".join([str(value) for value in self.feature_deviation])
        """
        print "    "*(indent+1) + "terminal list:"
        for terminal in self.terminal_set:
            terminal.show(indent+2)

        print "    "*(indent+1) + "nonterminal list:"
        for nonterminal in self.nonterminal_set:
            nonterminal.show(indent+2)
            
        print "    "*(indent+1) + "start symbol:"
        self.start_symbol.show(indent+2)

        print "    "*(indent+1) + "rule:"
        for nonterminal, rule_set in self.rule_dict.iteritems():
            for rule in rule_set:
                rule.show(indent+2)
            print ""
        """
        print "    "*(indent+1) + "right-hand-side rule:"
        for nonterminal, rule_index_set in self.rhs_rule_dict.iteritems():
            nonterminal.show(indent+2)
            for rule, index in rule_index_set:
                rule.show(indent+3)
                print "    "*(indent+3) + "index=%d" % index
        """
        return











        
