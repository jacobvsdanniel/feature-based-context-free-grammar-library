#!/usr/bin/python
# -*- coding: utf-8 -*-


import sys
from fscfg_learner import FSCFG_Learner

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: %s [training data directory]" % sys.argv[0]
        exit()

    learner = FSCFG_Learner(3)
    learner.read_training_data(sys.argv[1], max_size=100)
    learner.train(initial_size=3, batch_size=10, history_size=10)
    learner.grammar.show(0)
