import sys
import random

sample_type_list = ["100 100 100", "200 200 200", "300 300 300", "400 400 400"]
string_type_list = [[0,1,2,3], [1,2,3,0], [2,3,0,1], [3,0,1,2]]

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: %s [output directory] [number of sample strings]" % sys.argv[0]
        exit()

    output_directory = sys.argv[1]
    sample_strings = int(sys.argv[2])
    
    for string_index in range(1, sample_strings+1):
        with open("%s/input_%03d.txt" % (output_directory, string_index), "w") as f:
            string_type = random.randint(0, len(string_type_list)-1)
            for sample_type in string_type_list[string_type]:
                f.write("%s\n" % sample_type_list[sample_type])
