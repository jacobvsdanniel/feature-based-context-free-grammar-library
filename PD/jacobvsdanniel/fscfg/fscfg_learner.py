#!/usr/bin/python 
# -*- coding: utf-8 -*-

import re
import sys
import os 
import copy
import random
import numpy
import scipy
import scipy.stats
from collections import deque
from collections import defaultdict
from fscfg import Nonterminal, Terminal, Rule, FSCFG
from viterbi_parser import Viterbi_Parser
from sklearn.cluster import KMeans
        
class FSCFG_Learner(object):
    def __init__(self, features, expected_rule_length=1, scan_tolerance=0, scan_confidence=2.0):
        self.grammar = None
        self.features = features
        self.scan_tolerance = scan_tolerance
        
        # Store training data with 2 dim list indexed with [i][j], i: # strings to train, j: # samples in a string 
        self.training_data = []
        
        # Precompute distributions for speeding up
        self.rule_length_distribution = scipy.stats.poisson(expected_rule_length)
        self.scan_error = scipy.stats.multivariate_normal.logpdf([scan_confidence]*self.features, mean=[0.0]*self.features, cov=numpy.identity(self.features))
        return

    def read_training_data(self, input_path, normalize=True, max_size=1000):
        file_list = []
        for entry in os.listdir(input_path):
            entry_path = os.path.join(input_path, entry)
            if os.path.isfile(entry_path):
                file_list.append(entry_path)
        for file_path in sorted(file_list)[:max_size]:
            with open(file_path, "r") as f:
                sample_string = []
                for line in f.readlines():
                    string_list = line.strip().split(" ")
                    sample = [float(feature_value) for feature_value in string_list]
                    sample_string.append(sample)
                self.training_data.append(sample_string)
        
        self.max_string_length = max([len(string) for string in self.training_data])
        
        # Normalize each feature to standard gaussion distribution
        if normalize:
            sample_list = numpy.array([sample for sample_string in self.training_data for sample in sample_string], dtype=float)
            sample_mean = numpy.mean(sample_list, axis=0)
            sample_deviation = numpy.std(sample_list, axis=0)
            for index, sample_string in enumerate(self.training_data):
                self.training_data[index] = (numpy.array(sample_string, dtype=float) - sample_mean) / sample_deviation
            self.feature_covariance = numpy.identity(self.features, dtype=float) / 10.0
        else:
            sample_list = numpy.array([sample for sample_string in self.training_data for sample in sample_string], dtype=float)
            self.feature_covariance = numpy.diag(numpy.var(sample_list, axis=0))
        
        # Quantize data
        self.merge_samples()
        
        # Precompute distributions for speeding up
        #self.rule_probability_distribution = [None] + [scipy.stats.dirichlet([1.0/i]*i) for i in range(1, len(self.training_data)+1)]
        self.rule_count_probability = [None] + [scipy.stats.dirichlet([1.0]*i).pdf([1.0/i]*i) for i in range(1, len(self.training_data)+1)]
        return
    
    def merge_samples(self, quantiles=20, improvement_threshold=0.001):
        """ Apply k-means to merge similar samples """
        # Copy training sample strings (numpy arrays) to sample list (a list of lists)
        sample_list = [string.tolist() for string in self.training_data]
        string_length_list = [0] + [len(string) for string in sample_list]
        index_cut_list = numpy.cumsum(string_length_list)
        sample_list = reduce(lambda string_1, string_2: string_1 + string_2, sample_list)
        
        # Find the best number of clusters
        best_kmeans = None
        improvement_threshold = improvement_threshold * self.features * len(sample_list)
        print "threshold=%g" % improvement_threshold
        max_clusters = min(quantiles**self.features, len(sample_list))
        for clusters in range(1, max_clusters+1):
            kmeans = KMeans(n_clusters=clusters)
            kmeans.fit(sample_list)
            print kmeans.inertia_
            if best_kmeans and best_kmeans.inertia_ - kmeans.inertia_ < improvement_threshold: break
            best_kmeans = kmeans
            
        # Use clustered centers as samples
        sample_list = [best_kmeans.cluster_centers_[label] for label in best_kmeans.labels_]
        
        # Re-partition sample list to sample strings
        self.training_data = [numpy.array(sample_list[index_cut_list[i]:index_cut_list[i+1]]) for i in range(len(index_cut_list)-1)]
        return
    
    def trace_parse_tree(self, tree, sample_string, sample_index, leaf_terminal_set):
        symbol = tree[0][0]
        
        # Node: terminal
        if symbol in self.grammar.terminal_set:
            leaf_terminal_set.add(symbol)
            symbol.add_sample(sample_string[sample_index[0]], self.grammar.sample_to_terminal)
            sample_index[0] = sample_index[0] + 1
            return
        
        rule = tree[0][1]
        
        # Node: nonterminal to epsilon
        if not rule: return
                
        # Node: nonterminal with normal rule or skip rule
        if True:#rule not in self.grammar.skip_rule_set:
            # Normal rules
            rhs = []
            for child_tree in tree[1]:
                child_symbol = child_tree[0][0]
                if child_symbol in self.grammar.terminal_set or child_tree[0][1]:
                    rhs.append(child_symbol)
                    self.trace_parse_tree(child_tree, sample_string, sample_index, leaf_terminal_set)
                
            if len(rhs) == len(rule.right):
                rule.count = rule.count + 1.0
                rule.left.count = rule.left.count + 1.0
            else:
                # Some child nonterminals expand to epsilon
                modified_rule = rule.get_clone()
                modified_rule.right = rhs
                modified_rule.count = 1.0
                modified_rule.left.count = modified_rule.left.count + 1.0
                modified_rule.compute_hash()
                if modified_rule not in self.grammar.rule_dict[modified_rule.left]:
                    self.grammar.insert_rule(modified_rule)
                else:
                    existing_rule = self.grammar.name_to_rule[modified_rule.name]
                    existing_rule.count = existing_rule.count + modified_rule.count
        """
        else:
            # N -> SKIP N or S -> S SKIP
            if rule.right[0] == self.grammar.skip_nonterminal:
                skip_rhs_index = 0
                skip_sample_index = sample_index[0]
                sample_index[0] = sample_index[0] + 1
            else:
                skip_rhs_index = 1
                skip_sample_index = sample_index[1]
                sample_index[1] = sample_index[1] - 1
            
            new_lexical_rule = self.grammar.add_lexical_rule(sample_string[skip_sample_index])
            modified_rule = rule.get_clone()
            modified_rule.right[skip_rhs_index] = new_lexical_rule.left
            modified_rule.count = 1.0
            modified_rule.left.count = modified_rule.left.count + 1.0
            
            child_tree = tree[1][1-skip_rhs_index]
            if child_tree[0][1]:
                self.trace_parse_tree(child_tree, sample_string, sample_index, leaf_terminal_set)
            else:
                # when child preterminal expand to epsilon
                modified_rule.right = [new_lexical_rule.left]
            modified_rule.compute_hash()
            self.grammar.insert_rule(modified_rule)
        """
        return
    
    def train(self, initial_size=3, batch_size=10, history_size=10):
        self.grammar = FSCFG(features=self.features)
        self.grammar.start_symbol = self.grammar.add_nonterminal()
        
        # Initialize grammar with naive rules and optimize it
        for sample_string in self.training_data[:initial_size]:
            self.grammar.add_naive_rule(sample_string)
        self.grammar.compute_rule_probability()
        self.grammar.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in self.grammar.nonterminal_set}
        probability_list = self.best_first_search(self.training_data[:initial_size])
        print "***** Initial Grammar *****\n\n\n"
        self.grammar.show(0)
        
        # Use remaining sample strings batch by batch
        batch_index = initial_size
        while batch_index < len(self.training_data):
            # Concatenate indices of new strings of current batch and old strings
            new_strings = min(batch_size, len(self.training_data)-batch_index)
            old_strings = min(history_size, batch_index)
            string_index_list = range(batch_index, batch_index+new_strings) + random.sample(range(batch_index), old_strings)
            fail_index_list = []
            
            print "\n\n\n***** Batch start at %d *****\n\n\n" % batch_index
            print string_index_list
            print probability_list
            print numpy.percentile(probability_list, 0)-0.01
            
            # Update grammar if a new string is recognized
            for list_index, string_index in enumerate(string_index_list):
                sample_string = self.training_data[string_index]
                #self.grammar.add_skip_and_epsilon(self.max_string_length, self.scan_error)
                self.grammar.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in self.grammar.nonterminal_set}
                parse_threshold = numpy.percentile(probability_list, 0)-0.01 + self.scan_tolerance * self.scan_error
                parser = Viterbi_Parser(self.grammar, parse_threshold=parse_threshold)
                parser.sample_string = sample_string
                parser.initialize_chart()
                recognize, probability = parser.parse()
                
                print "\nParse data_%d: %g" % (string_index, probability)
                if recognize: parser.show_parse_tree(parser.parse_tree, 1)
                
                if not recognize:
                    fail_index_list.append(string_index)
                elif list_index < new_strings:
                    leaf_terminal_set = set()
                    self.trace_parse_tree(parser.parse_tree, sample_string, [0,-1], leaf_terminal_set)
                    """
                    for terminal in leaf_terminal_set:
                        terminal.merge_samples()
                    """
                #self.grammar.remove_skip_and_epsilon()
                self.grammar.compute_rule_probability()
            
            print fail_index_list
            
            # Add naive rules for nonrecognized strings, and then optimize grammar by greedy search
            if fail_index_list:
                for string_index in fail_index_list:
                    self.grammar.add_naive_rule(self.training_data[string_index])
                self.grammar.compute_rule_probability()
                self.grammar.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in self.grammar.nonterminal_set}
                probability_list = self.best_first_search([self.training_data[i] for i in string_index_list])
                self.grammar.show(0)
            
            batch_index = batch_index + batch_size
        
        # Re-parse all strings
        probability_list = []
        #self.grammar.add_skip_and_epsilon(self.max_string_length, self.scan_error)
        self.grammar.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in self.grammar.nonterminal_set}
        for sample_string in self.training_data:
            parser = Viterbi_Parser(self.grammar)
            parser.sample_string = sample_string
            parser.initialize_chart()
            recognize, probability = parser.parse()
            
            print ""
            print sample_string
            print "recognize=%s probability=%g" % (recognize, probability)
            
            if recognize:
                parser.show_parse_tree(parser.parse_tree, 1)
                probability_list.append(probability)
        #self.grammar.remove_skip_and_epsilon()
        print "Recognition rate: (%d/%d)=%g" % (len(probability_list), len(self.training_data), float(len(probability_list))/len(self.training_data))
        print "probability rank [0, 25, 50, 75, 100]:"
        print numpy.percentile(probability_list, [0,25,50,75,100])
        return numpy.percentile(probability_list, 50)

    def get_grammar_usage_of_parse_tree(self, tree, sample_index, terminal_usage, rule_usage, nonterminal_usage):
        symbol = tree[0][0]
        rule = tree[0][1]
        child_list = tree[1]
        
        if symbol in self.grammar.terminal_set:
            terminal_usage[symbol].add(sample_index[0])
            sample_index[0] = sample_index[0] + 1
        else:
            rule_usage[rule] = rule_usage[rule] + 1
            nonterminal_usage.add(rule.left)
            for child in child_list:
                self.get_grammar_usage_of_parse_tree(child, sample_index, terminal_usage, rule_usage, nonterminal_usage)
        return
    
    def best_first_search(self, training_data):
        print "\n\n<BFS>\n\n"
        while True:
            # Evaluate grammar and store usage
            probability_list = [float("-inf") for i in range(len(training_data))]
            terminal_usage_list = [defaultdict(lambda: set()) for i in range(len(training_data))]
            rule_usage_list = [defaultdict(lambda: 0) for i in range(len(training_data))]
            nonterminal_usage = set()
            preterminal_set = self.grammar.get_preterminal_set()
            
            for string_index, sample_string in enumerate(training_data):
                parser = Viterbi_Parser(self.grammar)
                parser.sample_string = sample_string
                parser.initialize_chart()
                recognize, probability_list[string_index] = parser.parse()
                self.get_grammar_usage_of_parse_tree(parser.parse_tree, [0], terminal_usage_list[string_index], rule_usage_list[string_index], nonterminal_usage)
            
            min_cost = self.evaluate_grammar(self.grammar, probability_list)
            better_grammar = None
            affected = None
            
            # Store action as ["merge"/"chunk", new_nonterminal, merged_terminal, old_nonterminal_list]
            action = [None, None, None, []]
            
            # Search all merge actions
            for N_i in self.grammar.nonterminal_set:
                nonterminal_usage = nonterminal_usage - set([N_i])
                for N_j in nonterminal_usage:
                    # A preterminal can only merge with another preterminal
                    #if (N_i in preterminal_set) != (N_j in preterminal_set): continue
                    if N_i not in preterminal_set or N_j not in preterminal_set: continue
                    current_grammar = self.grammar.get_clone()
                    N_z, affected_terminal_dict, affected_rule_dict = current_grammar.merge(self.grammar.name_to_symbol, N_i, N_j)
                    current_grammar.compute_rule_probability()
                    current_grammar.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in current_grammar.nonterminal_set}
                    affected_probability_list = self.get_affected_probablility(training_data, probability_list, terminal_usage_list, rule_usage_list, affected_terminal_dict, affected_rule_dict)
                    if affected_probability_list:
                        current_cost = self.evaluate_grammar(current_grammar, affected_probability_list)
                    else:
                        current_cost = float("inf")
                    if min_cost > current_cost:
                        better_grammar = current_grammar
                        min_cost = current_cost
                        if affected_terminal_dict:
                            new_terminal = affected_terminal_dict.values()[0]
                        else:
                            new_terminal = None
                        affected = affected_probability_list
                        action = ["merge", N_z, new_terminal, [N_i, N_j]]
            """
            # Search all chunk actions
            for nonterminal in self.grammar.nonterminal_set:
                for rule in self.grammar.rule_dict[nonterminal]:
                    for rhs_i in range(len(rule.right)-1):
                        for rhs_j in range(rhs_i+1, len(rule.right)):
                            current_grammar = self.grammar.get_clone()
                            symbol_string = [current_grammar.name_to_symbol[symbol.name] for symbol in rule.right[rhs_i:rhs_j+1]]
                            N_z, chunks, affected_rule_dict = current_grammar.chunk(symbol_string)
                            current_grammar.compute_rule_probability()
                            current_grammar.epsilon_rule_probability = {nonterminal: float("-inf") for nonterminal in current_grammar.nonterminal_set}
                            if chunks < 2: continue
                            affected_probability_list = self.get_affected_probablility(training_data, probability_list, None, rule_usage_list, {}, affected_rule_dict)
                            current_cost = self.evaluate_grammar(current_grammar, affected_probability_list)
                            if min_cost > current_cost:
                                better_grammar = current_grammar
                                min_cost = current_cost
                                action = ["chunk", N_z, None, symbol_string]
            """                 
            if not better_grammar: break
            
            # Show original grammar
            self.grammar.show(0)
            discription_length = self.grammar.get_discription_length(self.rule_length_distribution, self.rule_count_probability)
            print "DL=%g" % discription_length
            print "parse probabilities:"
            print probability_list
            cost = discription_length * 0.01 - numpy.log(numpy.mean(numpy.exp(probability_list)))
            print "cost=%g" % cost
            print ""
            
            # Only merge samples once per iteration
            if action[2]:
                #action[2].merge_samples()
                action[2].show(1)
            # Show change
            print action[0]
            action[1].show(1)
            for symbol in action[3]:
                symbol.show(2)
            
            # Update grammar
            self.grammar = better_grammar
            
            # Evaluate new grammar
            discription_length = self.grammar.get_discription_length(self.rule_length_distribution, self.rule_count_probability)
            print "DL=%g" % discription_length
            print "parse probabilities:"
            print affected
            cost = discription_length * 0.01 - numpy.log(numpy.mean(numpy.exp(affected)))
            print "cost=%g" % cost
            print ""
            
        return probability_list

    def get_affected_probablility(self, training_data, probability_list, terminal_usage_list, rule_usage_list, affected_terminal_dict, affected_rule_dict):
        affected_probability_list = probability_list[:]
        
        for string_index, sample_string in enumerate(training_data):
           
            for affected_terminal, new_terminal in affected_terminal_dict.iteritems():
                if affected_terminal not in terminal_usage_list[string_index]: continue
                for sample_index in terminal_usage_list[string_index][affected_terminal]:
                    # TODO: store scan probability in scanner
                    old_scan_probability = affected_terminal.scan(sample_string[sample_index], self.feature_covariance)
                    new_scan_probability = new_terminal.scan(sample_string[sample_index], self.feature_covariance)
                    #if new_scan_probability < self.scan_error: return None
                    affected_probability_list[string_index] = affected_probability_list[string_index] - old_scan_probability + new_scan_probability
            
            for affected_rule, new_rule in affected_rule_dict.iteritems():
                if affected_rule not in rule_usage_list[string_index]: continue
                affected_probability_list[string_index] = affected_probability_list[string_index] + (new_rule.probability - affected_rule.probability) * rule_usage_list[string_index][affected_rule]
            
        return affected_probability_list
        
    def evaluate_grammar(self, grammar, probability_list, prior_weight=0.01):
        discription_length = grammar.get_discription_length(self.rule_length_distribution, self.rule_count_probability)
        #cost = discription_length * prior_weight - numpy.log(numpy.mean(numpy.exp(probability_list)))
        cost = discription_length * prior_weight - numpy.sum(probability_list)
        return cost











