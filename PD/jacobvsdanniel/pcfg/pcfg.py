#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import sys
import copy
import numpy
import operator

class Symbol(object):
    def __init__(self, value):
        self.value = value
        return
        
    def __eq__(self, other):
        return self.value == other.value
        
    def __ne__(self, other):
        return not (self == other)
        
    def __hash__(self):
        return hash(self.value)
        
    def show(self, indent):
        print "    "*indent + "symbol: [%s]" % self.value

class Rule(object):
    def __init__(self):
        self.left = None
        self.right = []
        self.probability = None
        
    def __eq__(self, other):
        return (self.left == other.left) and (self.right == other.right)
        
    def __ne__(self, other):
        return not (self == other)
    
    def compute_hash(self):
        self.hash_value = hash((self.left, hash(tuple(self.right)), self.probability))
        
    def __hash__(self):
        return self.hash_value
    
    def read_rule_line(self, line):
        string_list = line.strip().split(" ")
        self.left = Symbol(string_list[0])
        for string in string_list[1:-1]:
            self.right.append(Symbol(string))
        self.probability = float(string_list[-1])
        
    def show(self, indent):
        output = "    "*indent + "%s ->" % self.left.value
        for symbol in self.right:
            output = output + " %s" % symbol.value
        output = output + " [%g]" % self.probability
        print output
        """
        print "    "*indent + "left:"
        self.left.show(indent+1)
        print "    "*indent + "right:"
        for symbol in self.right: symbol.show(indent+1)
        print "    "*indent + "probability: [%f]" % self.probability
        """
        return
        
class PCFG(object):
    def __init__(self, name):
        self.name = name
        self.terminal_set = set()
        self.nonterminal_set = set()
        self.start_symbol = None
        self.rule_dict = {}
        
        # A 2d numpy array representing probabilistic reflexive and transitive left corner relation
        self.left_relation = None

        # 1d numpy arrays storing epsilon probabilities
        self.epsilon_rule_probability = None
        self.epsilon_expansion_probability = None
        
        # The mapping for the index of self.left_relation
        self.nonterminal_to_order = {}
        self.order_to_nonterminal = {}
        
        return
    
    def read_terminal_file(self, input_file):
        self.terminal_set.clear()
        f = open(input_file, "r")
        for line in f.readlines():
            symbol = Symbol(line.strip())
            self.terminal_set.add(symbol)
        f.close()
        return
        
    def read_nonterminal_file(self, input_file):
        self.nonterminal_set.clear()
        index = -1
        f = open(input_file, "r")
        for line in f.readlines():
            index = index + 1
            symbol = Symbol(line.strip())
            self.nonterminal_set.add(symbol)
            self.nonterminal_to_order[symbol] = index
            self.order_to_nonterminal[index] = symbol
        f.close()
        return
        
    def read_start_symbol_file(self, input_file):
        self.start_symbol = None
        with open(input_file, "r") as f:
            symbol = Symbol(f.readline().strip())
        if symbol not in self.nonterminal_set:
            raise AssertionError("Start symbol [%s] not in nonterminal set" % symbol.value)
        self.start_symbol = symbol
        return    
        
    def read_rule_file(self, input_file):
        """ Not storing epsilon rules in rule_dict """
        self.epsilon_rule_probability = numpy.zeros(len(self.nonterminal_set), dtype=float)
        self.rule_dict = {}
        rule_dict = {}
        
        f = open(input_file, "r")
        for line in f.readlines():
            rule = Rule()
            rule.read_rule_line(line)
            rule.compute_hash()
            
            if rule.left not in self.nonterminal_set:
                raise AssertionError("LHS [%s] not in nonterminal set" % rule.left.value)
            for symbol in rule.right:
                if (symbol not in self.nonterminal_set) and (symbol not in self.terminal_set):
                    raise AssertionError("RHS [%s] not in nonterminal or terminal set" % symbol.value)
                
            if not rule.right:
                self.epsilon_rule_probability[self.nonterminal_to_order[rule.left]] = rule.probability
            elif rule.left not in rule_dict:
                rule_dict[rule.left] = set([rule])
            else:
                rule_dict[rule.left].add(rule)
        f.close()
        
        for symbol, rule_set in rule_dict.iteritems():
            total_probability = sum([rule.probability for rule in rule_set])
            total_probability = total_probability + self.epsilon_rule_probability[self.nonterminal_to_order[symbol]]
            if not numpy.isclose(total_probability, 1):
                raise AssertionError("Total probability for LHS [%s] is %g, not unity" % (symbol.value, total_probability))
                
        self.rule_dict = rule_dict
        return

    def compute_left_relation(self):
        nonterminals = len(self.nonterminal_set)
        self.left_relation = numpy.zeros((nonterminals,nonterminals), dtype=float)

        # record the intermediate nonterminal index in the path of left corner relation closure
        intermediate = -1 * numpy.ones((nonterminals, nonterminals), dtype=int)

        # record the rule with the highest probability such that two nonterminals has left corner relation
        applyed_rule = [[None for i in range(nonterminals)] for j in range(nonterminals)]
        
        # Compute probabilistic left corner relation
        for nonterminal in self.nonterminal_set:
            for rule in self.rule_dict[nonterminal]:
                if rule.right[0] in self.nonterminal_set:
                    row = self.nonterminal_to_order[nonterminal]
                    column = self.nonterminal_to_order[rule.right[0]]
                    if rule.probability > self.left_relation[row][column]:
                        self.left_relation[row][column] = rule.probability
                        applyed_rule[row][column] = rule

        shortest_path_matrix = -numpy.ma.log(self.left_relation)
        shortest_path_matrix = shortest_path_matrix.filled(float("inf"))
        
        # apply Floyd-Warshall Algorithm to find shortest path 
        for intermediate_vertex in range(nonterminals):
            for vertex_i in range(nonterminals):
                for vertex_j in range(nonterminals):
                    new_path = shortest_path_matrix[vertex_i][intermediate_vertex] + shortest_path_matrix[intermediate_vertex][vertex_j]
                    if shortest_path_matrix[vertex_i][vertex_j] > new_path:
                        shortest_path_matrix[vertex_i][vertex_j] = new_path
                        intermediate[vertex_i][vertex_j] = intermediate_vertex

        self.left_relation = numpy.exp(-shortest_path_matrix)
        numpy.fill_diagonal(self.left_relation, 1)
        print self.left_relation

        # show each shortest path
        for i in range(nonterminals):
            for j in range(nonterminals):
                if i != j and self.left_relation[i][j] > 0:
                    print "from %s to %s with prob=%g" % (self.order_to_nonterminal[i].value, self.order_to_nonterminal[j].value, self.left_relation[i][j])
                    self.show_maximum_left_relation_path(i, j, intermediate, applyed_rule)
                    print "-------------"
        return 

    def show_maximum_left_relation_path(self, start, end, intermediate, applyed_rule):
        if intermediate[start][end] == -1:
            applyed_rule[start][end].show(0)
            return 

        self.show_maximum_left_relation_path(start, intermediate[start][end], intermediate, applyed_rule)
        self.show_maximum_left_relation_path(intermediate[start][end], end, intermediate, applyed_rule)
        return 
    """
    def compute_left_relation(self):
        nonterminals = len(self.nonterminal_set)
        self.left_relation = numpy.zeros((nonterminals,nonterminals), dtype=float)
        
        # Compute probabilistic left corner relation
        for nonterminal in self.nonterminal_set:
            for rule in self.rule_dict[nonterminal]:
                if rule.right[0] in self.nonterminal_set:
                    row = self.nonterminal_to_order[nonterminal]
                    column = self.nonterminal_to_order[rule.right[0]]
                    self.left_relation[row][column] = self.left_relation[row][column] + rule.probability
        #print self.left_relation
        
        # Compute reflexive and transitive relation
        identity_relation = numpy.identity(nonterminals)
        self.left_relation = numpy.linalg.inv(identity_relation - self.left_relation)
        print self.left_relation
        
        return
    """
    def compute_epsilon_expansion(self, improvement_bound=1e-10):
        nonterminals = len(self.nonterminal_set)
        self.epsilon_expansion_probability = numpy.copy(self.epsilon_rule_probability)
        applyed_rule = [None for i in range(nonterminals)]
        rule_list = [[] for i in range(nonterminals)]
        
        # Store a list of epsilon probabilty terms for each nonterminal
        # if index(X,Y,Z,U,V) = (1,2,3,4,5) and e_X = P(X->e) + P(X->YZ)*e_Y*e_Z + P(X->UV)*e_U*e_V
        # then equation[1] = [[P(X->YZ),[2,3]], [P(X->UV),[4,5]]]
        equation = [[] for i in range(nonterminals)]
        for nonterminal in self.nonterminal_set:
            for rule in self.rule_dict[nonterminal]:
                has_terminal = False
                for symbol in rule.right:
                    if symbol in self.terminal_set:
                        has_terminal = True
                        break
                if not has_terminal:
                    nonterminal_index = self.nonterminal_to_order[nonterminal]
                    rhs_index = [self.nonterminal_to_order[nonterminal] for nonterminal in rule.right]
                    equation[nonterminal_index].append([rule.probability, rhs_index])
                    rule_list[nonterminal_index].append(rule)

        # Compute epsilon expansion 
        while True:
            print self.epsilon_expansion_probability
            converge = True
            for index in range(nonterminals):
                update_value = self.epsilon_rule_probability[index]
                applyed_rule[index] = None
                for term_index in range(len(equation[index])):
                    #update_value = max(update_value, term[0] * numpy.prod(self.epsilon_expansion_probability[term[1]]))
                    term = equation[index][term_index]
                    if update_value < term[0] * numpy.prod(self.epsilon_expansion_probability[term[1]]):
                        update_value = term[0] * numpy.prod(self.epsilon_expansion_probability[term[1]])
                        applyed_rule[index] = rule_list[index][term_index]
                if update_value - self.epsilon_expansion_probability[index] > improvement_bound:
                    converge = False
                self.epsilon_expansion_probability[index] = update_value 
            if converge: break

        print self.epsilon_expansion_probability

        for index in range(nonterminals):
            if self.epsilon_expansion_probability[index] > 0:
                print "from %s to epsilon with prob=%g" % (self.order_to_nonterminal[index].value, self.epsilon_expansion_probability[index])
                if applyed_rule[index]:
                    applyed_rule[index].show(0)
                else:
                    print "%s-> [%g]" % (self.order_to_nonterminal[index].value, self.epsilon_expansion_probability[index])

        return
    """
    def compute_epsilon_expansion(self, improvement_bound=1e-10):
        nonterminals = len(self.nonterminal_set)
        self.epsilon_expansion_probability = numpy.copy(self.epsilon_rule_probability)
        
        # Store a list of epsilon probabilty terms for each nonterminal
        # if index(X,Y,Z,U,V) = (1,2,3,4,5) and e_X = P(X->e) + P(X->YZ)*e_Y*e_Z + P(X->UV)*e_U*e_V
        # then equation[1] = [[P(X->YZ),[2,3]], [P(X->UV),[4,5]]]
        equation = [[] for i in range(nonterminals)]
        for nonterminal in self.nonterminal_set:
            for rule in self.rule_dict[nonterminal]:
                has_terminal = False
                for symbol in rule.right:
                    if symbol in self.terminal_set:
                        has_terminal = True
                        break
                if not has_terminal:
                    nonterminal_index = self.nonterminal_to_order[nonterminal]
                    rhs_index = [self.nonterminal_to_order[nonterminal] for nonterminal in rule.right]
                    equation[nonterminal_index].append([rule.probability, rhs_index])

        # Compute epsilon expansion 
        while True:
            #print self.epsilon_expansion_probability
            converge = True
            for index in range(nonterminals):
                update_value = self.epsilon_rule_probability[index]
                for term in equation[index]:
                    update_value = update_value + term[0] * numpy.prod(self.epsilon_expansion_probability[term[1]])
                if update_value - self.epsilon_expansion_probability[index] > improvement_bound:
                    converge = False
                self.epsilon_expansion_probability[index] = update_value 
            if converge: break

        print self.epsilon_expansion_probability
        return
    """
    def show(self, indent):
        print "    "*indent + "name: [%s]" % self.name
        
        print "    "*(indent+1) + "terminal:"
        for symbol in self.terminal_set:
            symbol.show(indent+2)
        
        print "    "*(indent+1) + "nonterminal:"
        for symbol in self.nonterminal_set:
            symbol.show(indent+2)
        
        print "    "*(indent+1) + "start symbol:"
        self.start_symbol.show(indent+2)
        
        print "    "*(indent+1) + "rule:"
        for symbol, rule_set in self.rule_dict.iteritems():
            symbol.show(indent+2)
            for rule in rule_set:
                rule.show(indent+3)
        
        return
        
        
        
        
        
        
        
        
        
        
        
        
