import re
import sys
import copy
from pcfg import *

class State(object):
    def __init__(self, rule=None, next_index=None, start_index=None, next_rhs_index=None, forward_probability=None, inner_probability=None):
        self.rule = copy.deepcopy(rule)
        
        # Index of next symbol to scan in input string (i)
        self.next_index = next_index
        
        # Index of the start of LHS in input string (k)
        self.start_index = start_index
        
        # Index of next right-hand-side symbol (.)
        self.next_rhs_index = next_rhs_index
        
        # Forward probability (alpha)
        self.forward_probability = forward_probability
        
        # Inner probability (gamma)
        self.inner_probability = inner_probability        
        return
        
    def __eq__(self, other):
        return (self.rule == other.rule) and (self.next_index == other.next_index) and (self.start_index == other.start_index) and (self.next_rhs_index == other.next_rhs_index)
        
    def __ne__(self, other):
        return not (self == other)
    
    def __hash__(self):
        return hash((self.rule, self.next_index, self.start_index, self.next_rhs_index))
        
    def show(self, indent):
        output = "    "*indent + "%d:%d_%s ->" % (self.next_index, self.start_index, self.rule.left.value)
        find_dot = False
        for i in range(len(self.rule.right)):
            if (not find_dot) and (i==self.next_rhs_index):
                output = output + " ."
                find_dot = True
            output = output + " %s" % self.rule.right[i].value
        if not find_dot: output = output + " ."
        output = output + " [%g, %g]" % (self.forward_probability, self.inner_probability)
        print output
        return
    
class Sentence_Parser(object):
    def __init__(self, grammar=None):
        self.grammar = grammar
        return

    def set_input_string(self, line):
        self.symbol_string = [Symbol(string) for string in line.strip().split(" ")]
        
    def initialize_chart(self):
        # chart[0] ~ chart[string_length]
        string_length = len(self.symbol_string)
        self.state_chart = [[] for i in range(string_length+1)]
        
        # Add dummy state 0:0_""->.S[1,1]
        dummy_symbol = Symbol("")
        dummy_rule = Rule()
        dummy_rule.read_rule_line("S S 1")
        dummy_rule.left = dummy_symbol
        dummy_rule.compute_hash()
        dummy_state = State(rule=dummy_rule, next_index=0, start_index=0, next_rhs_index=0, forward_probability=1, inner_probability=1)
        self.state_chart[0].append(dummy_state)
        
        # Add a "list of dict{state:index}" for fast existence testing and index lookup
        self.state_to_index = [{} for i in range(string_length+1)]
        self.state_to_index[0][dummy_state] = 0
        
        return

    def show(self, indent):
        self.grammar.show(indent)
        
        line = "    "*indent + "symbol string:"
        for symbol in self.symbol_string:
            line = line + " %s" % symbol.value
        print line
        
        print "    "*indent + "chart:"
        for i in range(len(self.state_chart)):
            print "    "*(indent+1) + "[%d]" % i
            for state in self.state_chart[i]:
                state.show(indent+2)
        
        """
        print "    "*indent + "state to index:"
        for i in range(len(self.state_to_index)):
            print "    "*(indent+1) + "[%d]" % i
            for state, index in self.state_to_index[i].iteritems():
                state.show(indent+2)
                print "    "*(indent+2) + "(index=%d)" % index
        """
        
        return

    def predictor(self, state):
        nonterminal = state.rule.right[state.next_rhs_index]
        
        for rule in self.grammar.rule_dict[nonterminal]:
            predicted_state = State()
            predicted_state.rule = rule
            predicted_state.next_index = state.next_index
            predicted_state.start_index = state.next_index
            predicted_state.next_rhs_index = 0
            predicted_state.forward_probability = state.forward_probability * rule.probability
            predicted_state.inner_probability = rule.probability
            if predicted_state not in self.state_to_index[predicted_state.next_index]:
                self.state_to_index[predicted_state.next_index][predicted_state] = len(self.state_chart[predicted_state.next_index])
                self.state_chart[predicted_state.next_index].append(predicted_state)
            else:
                index = self.state_to_index[predicted_state.next_index][predicted_state]
                self.state_chart[predicted_state.next_index][index].forward_probability = self.state_chart[predicted_state.next_index][index].forward_probability + predicted_state.forward_probability
        return
    
    def scanner(self, state):
        if state.rule.right[state.next_rhs_index] != self.symbol_string[state.next_index]: return
        scanned_state = copy.deepcopy(state)
        scanned_state.next_index = state.next_index + 1
        scanned_state.next_rhs_index = state.next_rhs_index + 1
        
        if scanned_state not in self.state_to_index[scanned_state.next_index]:
            self.state_to_index[scanned_state.next_index][scanned_state] = len(self.state_chart[scanned_state.next_index])
            self.state_chart[scanned_state.next_index].append(scanned_state)
        return

    def completer(self, state):
        #state.show(4)
        for old_state in self.state_chart[state.start_index]:
            #old_state.show(5)
            if (old_state.next_rhs_index == len(old_state.rule.right)) or (old_state.rule.right[old_state.next_rhs_index] != state.rule.left): continue
            completed_state = copy.deepcopy(old_state)
            completed_state.next_index = state.next_index
            completed_state.next_rhs_index = completed_state.next_rhs_index + 1
            completed_state.forward_probability = old_state.forward_probability * state.inner_probability
            completed_state.inner_probability = old_state.inner_probability * state.inner_probability
            
            if completed_state not in self.state_to_index[completed_state.next_index]:
                self.state_to_index[completed_state.next_index][completed_state] = len(self.state_chart[completed_state.next_index])
                self.state_chart[completed_state.next_index].append(completed_state)
            else:
                index = self.state_to_index[completed_state.next_index][completed_state]
                self.state_chart[completed_state.next_index][index].forward_probability = self.state_chart[completed_state.next_index][index].forward_probability + completed_state.forward_probability
                self.state_chart[completed_state.next_index][index].inner_probability = self.state_chart[completed_state.next_index][index].inner_probability + completed_state.inner_probability
        return
    
    def parse(self):
        is_last_row = False
        for state_list in self.state_chart:
            for state in state_list:
                if state.next_rhs_index == len(state.rule.right):
                    if state.rule.left == Symbol(""): 
                        return True, state.inner_probability
                    self.completer(state)
                elif not is_last_row:
                    if state.rule.right[state.next_rhs_index] in self.grammar.nonterminal_set:
                        self.predictor(state)
                    else:
                        self.scanner(state)
        return False, 0
